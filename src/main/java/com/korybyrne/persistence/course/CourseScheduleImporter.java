/*
Copyright (c) 2016 Julius T. Kosan

Permission is hereby granted, free of charge, to any person obtaining
a copy of this software and associated documentation files (the
"Software"), to deal in the Software without restriction, including
without limitation the rights to use, copy, modify, merge, publish,
distribute, sublicense, and/or sell copies of the Software, and to
permit persons to whom the Software is furnished to do so, subject to
the following conditions:

The above copyright notice and this permission notice shall be
included in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY
CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT,
TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
*/

package com.korybyrne.persistence.course;

import com.korybyrne.domain.course.Course;
import com.korybyrne.domain.course.CourseSchedule;
import com.korybyrne.domain.course.Section;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.xml.sax.Attributes;
import org.xml.sax.SAXException;
import org.xml.sax.helpers.DefaultHandler;

import javax.xml.parsers.ParserConfigurationException;
import javax.xml.parsers.SAXParser;
import javax.xml.parsers.SAXParserFactory;
import java.io.IOException;
import java.net.URL;
import java.util.ArrayDeque;
import java.util.Deque;
import java.util.Objects;

public class CourseScheduleImporter extends DefaultHandler {
    private final static Logger logger = LoggerFactory.getLogger(CourseScheduleImporter.class);

    private CourseSchedule courseSchedule;

    private Deque<String> elementStack;
    private Deque<Object> objectStack; // It would be ridiculous to generify this stack

    private StringBuffer textBuffer;

    public CourseScheduleImporter() { //asdfasdf
        this.courseSchedule = new CourseSchedule();

        this.elementStack = new ArrayDeque<>();
        this.objectStack = new ArrayDeque<>();

        this.textBuffer = new StringBuffer();
    }

    @Override
    public void characters(char[] buf, int offset, int len) {

        String sRaw = new String(buf, offset, len);

        String s = sRaw.trim();

        if (s.length() == 0) {
            return; // ignore white space
        }

        textBuffer.append(s);
    }

    @Override
    public void startElement(String uri, String localName, String qName, Attributes attributes) {
        //Push it in element stack
        this.elementStack.push(qName);

        if ("course".equalsIgnoreCase(qName)) {
            Course course = new Course();

            this.objectStack.push(course);
        } else if ("section".equalsIgnoreCase(qName)) {

            Section section = new Section();

            if (attributes != null) {
                String status = attributes.getValue("status");
                section.setStatus(status);

//                int seatsOpen = Integer.parseInt(attributes.getValue("seats_open"));
//                section.setSeatsOpen(seatsOpen);

//                int capacity = Integer.parseInt(attributes.getValue("capacity"));
//                section.setCapacity(capacity);

//                String honorsStatus = attributes.getValue("honors");
//                section.setIsHonors("true".equalsIgnoreCase(honorsStatus));
            }

            this.objectStack.push(section);
        } else if ("location".equalsIgnoreCase(qName)) {
            this.objectStack.push(new DaysTimesRoom());
        }
    }

    @Override
    public void endElement(String namespaceURI, String sName, // simple name
                           String qName // qualified name
    ) throws SAXException {

        String value = textBuffer.toString();

        if (this.objectStack.size() != 0) {
            processEndTags(value.trim(), qName);
        }

        this.elementStack.pop();
        textBuffer.delete(0, textBuffer.length());

    }

    private void processEndTags(String value, String qName) throws SAXException {
        Object currentObject  = this.objectStack.peek();
        String currentElement = Objects.requireNonNull(this.elementStack.peek());

        if (currentObject instanceof Course) {
            Course course = (Course) currentObject;

            switch (currentElement) {
                case "number":
                    course.setCourseCode(value);
                    break;
                case "name":
                    course.setName(value);
                    break;
            }
        } else if (currentObject instanceof Section) {
            Section section = (Section) currentObject;

            switch (currentElement) {
                case "number":
                    section.setNumber(value);
                    break;

                case "section": // append section to course
                    this.objectStack.pop();

                    Course course = Objects.requireNonNull((Course) this.objectStack.peek());
                    course.addSection(section);
                    break;
            }
        } else if (currentObject instanceof DaysTimesRoom) {
            DaysTimesRoom daysTimesRoom = (DaysTimesRoom) currentObject;

            switch (currentElement) {
                case "days":
                    try {
                        daysTimesRoom.setDays(value);
                    } catch (Exception e) {
                        SAXException saxException = new SAXException();
                        saxException.addSuppressed(e);
                        throw saxException;
                    }
                    break;

                case "start_time":
                    try {
                        daysTimesRoom.setStartTimeMilitary(value);
                    } catch (Exception e) {
                        SAXException saxException = new SAXException();
                        saxException.addSuppressed(e);
                        throw saxException;
                    }
                    break;

                case "end_time":
                    try {
                        daysTimesRoom.setDurationFromEndTimeMilitary(value);
                    } catch (Exception e) {
                        SAXException saxException = new SAXException();
                        saxException.addSuppressed(e);
                        throw saxException;
                    }
                    break;

                case "location": // break up into TimeBlocks and append
                    this.objectStack.pop();

                    Section section = Objects.requireNonNull((Section) this.objectStack.peek());
                    section.addClassMeetings(daysTimesRoom.toClassMeetingList());
                    break;
            }
        }

        if ("course".equals(qName)) {
            Course course = (Course) this.objectStack.pop();

            courseSchedule.addCourse(course);
        }
    }

    public static CourseSchedule importFromResource(String resourceName) throws IOException, SAXException, ParserConfigurationException {
        CourseScheduleImporter importer = new CourseScheduleImporter();

        SAXParserFactory saxFactory = SAXParserFactory.newInstance();
        SAXParser saxParser = saxFactory.newSAXParser();

        logger.debug(resourceName);

        saxParser.parse(importer.getClass().getResource(resourceName).getFile(), importer);

        return importer.courseSchedule;
    }

    public static void main(String[] args) throws Exception {
        CourseSchedule courseSchedule = importFromResource("/com/korybyrne/persistence/courses.xml");

        logger.debug(courseSchedule.toString());
    }
}
