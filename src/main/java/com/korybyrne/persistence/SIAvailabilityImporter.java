package com.korybyrne.persistence;

import com.korybyrne.domain.TimeBlock;
import com.korybyrne.domain.course.Course;
import com.korybyrne.domain.course.CourseSchedule;
import com.korybyrne.domain.si.RequestedHours;
import com.korybyrne.domain.si.SupplementalInstructor;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import org.apache.commons.io.IOUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.IOException;
import java.io.InputStream;
import java.nio.charset.StandardCharsets;
import java.time.DayOfWeek;
import java.time.Duration;
import java.time.LocalTime;
import java.util.*;
import javax.json.Json;
import javax.json.JsonObject;
import javax.json.JsonReader;

public class SIAvailabilityImporter {
    private static final Logger logger = LoggerFactory.getLogger(SIAvailabilityImporter.class);

    private static final Table.Position sessionsStart = new Table.Position(1, 1);
    private static final Table.Position sessionsEnd   = new Table.Position(16, 7);

    private static final Table.Position requestedMinimumHours = new Table.Position(1,10);
    private static final Table.Position requestedMaximumHours = new Table.Position(2,10);

    private static final Table.Position requestedCoursesStart = new Table.Position(5, 10);
    private static final Table.Position requestedCoursesEnd = new Table.Position(16, 10);

    private static final Table.Position inClassStart = new Table.Position(5, 11);
    private static final Table.Position inClassEnd   = new Table.Position(16, 10);

    private static final Table.Position namePosition = new Table.Position(0, 10);
    private static final Table.Position idPosition   = new Table.Position(3, 10);

    private static final int startHour = 6;

    public static SupplementalInstructor importCsvResource(String resourceName, CourseSchedule courseSchedule) throws IOException {
        FileInputStream inputStream = new FileInputStream(resourceName);
        List<String> rows = IOUtils.readLines(inputStream, StandardCharsets.UTF_8);
        
        int r = 0;
        String[][] data = new String[rows.size()][];
        System.out.println(rows.size());
        for (String row : rows) {
            data[r] = (row+",t").split(","); // trailing commas are removed from the array with String::split
            data[r] = Arrays.copyOfRange(data[r], 0, data[r].length-1); // remove ",t"
            r++;
        }

        Table<String> table = new Table<>(data);

        logger.debug("Table: {}", table);

        return parseTable(table, courseSchedule);
    }

    private static SupplementalInstructor parseTable(Table<String> table, CourseSchedule courseSchedule) {
        List<TimeBlock> busyTimeList = parseBusyTimes(table);
        List<Course> courseList = parseCourseSchedule(table, courseSchedule);
        TreeSet<String> requestedCourses = parseRequestedCourses(table);
        RequestedHours requestedHours = parseRequestedHours(table);
        String name = parseName(table);
        int id      = parseSsuId(table);

        SupplementalInstructor si = new SupplementalInstructor(name, id, requestedHours, busyTimeList, 
                courseList, requestedCourses);

        logger.debug(si.toString());

        return si;
    }

    private static List<TimeBlock> parseBusyTimes(Table<String> table) {
        String curBusyValue = "";
        List<TimeBlock> busyTimeList = new ArrayList<>();
        int curStartHour = -1;

        Iterator<Table.Cell<String>> sessionIterator = table.columnFirstIterator(sessionsStart, sessionsEnd);
        while(sessionIterator.hasNext()) {
            Table.Cell<String> cell = sessionIterator.next();
            int day = (cell.position.c == 1) ? 7 : cell.position.c - 1; // Monday starts the week in java
            int hour = cell.position.r;
            String busyValue = cell.data;

            logger.debug("Cell: {}", cell);
            if (!busyValue.toLowerCase().equals(curBusyValue) || hour == sessionsEnd.r) {
                if (!curBusyValue.isEmpty()) {
                    busyTimeList.add(new TimeBlock(
                            DayOfWeek.of(day),
                            LocalTime.of(curStartHour - sessionsStart.r + startHour, 00),
                            Duration.ofHours(hour - curStartHour)
                    ));
                }

                curStartHour = hour;
                curBusyValue = busyValue.toLowerCase();
            }
        }

        return busyTimeList;
    }

    private static List<Course> parseCourseSchedule(Table<String> table, CourseSchedule mainCourseSchedule) {
        List<Course> courseList = new ArrayList<>();
        Iterator<Table.Cell<String>> courseIterator = table.columnFirstIterator(requestedCoursesStart, requestedCoursesEnd);
        while (courseIterator.hasNext()) {
            Table.Cell<String> cell = courseIterator.next();
            String departmentCode = cell.data.toUpperCase().replaceAll("\\s+", "");

            if (departmentCode.equals("")) {
                break;
            }

            Optional<Course> course = mainCourseSchedule.getCourse(departmentCode);

            if (!course.isPresent()) {
                logger.debug("Course code: " + departmentCode + " is not in the course schedule.");
                continue;
            }

            courseList.add(course.get());
        }
        return courseList;
    }

    private static RequestedHours parseRequestedHours(Table<String> table) {
        String minimumHoursString = assertNotBlank(table.get(requestedMinimumHours).data, "Minimum Hours");
        String maximumHoursString = assertNotBlank(table.get(requestedMaximumHours).data, "Maximum Hours");

        return new RequestedHours(
                Duration.ofHours(Long.parseLong(minimumHoursString)),
                Duration.ofHours(Long.parseLong(maximumHoursString))
        );
    }

    private static String parseName(Table<String> table) {
        return assertNotBlank(table.get(namePosition).data, "Name");
    }

    private static int parseSsuId(Table<String> table) {
        String id = assertNotBlank(table.get(idPosition).data, "SSU ID");

        return Integer.parseInt(id);
    }
    
    private static TreeSet<String> parseRequestedCourses(Table<String> table) {
        TreeSet<String> requestedCourses = new TreeSet<>();
        
        Iterator<Table.Cell<String>> courseIterator = 
                table.columnFirstIterator(requestedCoursesStart, requestedCoursesEnd);

        while(courseIterator.hasNext()){
            Table.Cell<String> cell = courseIterator.next();
            String courseCode = cell.data.toUpperCase().replaceAll("\\s+", "");
            
            if(courseCode.equals(""))
                break;
            
            requestedCourses.add(courseCode);
        }
        return requestedCourses;
    }

    private static String assertNotBlank(String data, String name) {
        if (data.equals("")) {
            throw new AvailabilityParseException(name + " must not be blank!");
        }

        return data;
    }

    

    private static class AvailabilityParseException extends RuntimeException {
        public AvailabilityParseException(String message) {
            super(message);
        }
    }
}
