package com.korybyrne.persistence;

import com.korybyrne.domain.SISchedule;
import com.korybyrne.domain.Session;
import com.korybyrne.domain.course.Course;
import com.korybyrne.domain.course.CourseSchedule;
import com.korybyrne.domain.si.SupplementalInstructor;
import com.korybyrne.persistence.course.CourseScheduleImporter;
import java.io.File;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.IOException;
import java.time.DayOfWeek;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class SIScheduleGenerator {
    private final Logger logger = LoggerFactory.getLogger(getClass());

    public SISchedule generateSchedule(String courseXmlResourceName, File[] siAvailabilityFiles,int numSIs) {
        List<SupplementalInstructor> siList = new ArrayList<>(numSIs);
        List<Session> sessionList = new ArrayList<>(numSIs * 5 * 3); // 3 per day of week
        CourseSchedule courseSchedule;

        try {
            courseSchedule = CourseScheduleImporter.importFromResource(courseXmlResourceName);
            //logger.debug(courseSchedule.toString());
        } catch (Exception e) {
            logger.debug("Could not generate schedule: {}\n{}", e.toString(),
                    Arrays.stream(e.getStackTrace())
                            .map(StackTraceElement::toString)
                            .reduce("", String::concat));
            return null;
        }

        int id = 0;
        for (int i = 0; i < numSIs; i++) {
            SupplementalInstructor si;
            try {
                /*
                *********************
                ***DON'T FORGET ME***
                *********************
                *///this is a hardcoded piece that needs to take user input
                si = SIAvailabilityImporter.importCsvResource(
                        siAvailabilityFiles[i].getPath(), courseSchedule);
            } catch (IOException e) {
                e.printStackTrace();
                return null;
            }

            List<Session> siSessionList = new ArrayList<>();

            for (DayOfWeek day : DayOfWeek.values()) {
                if (day == DayOfWeek.SUNDAY || day == DayOfWeek.SATURDAY) { continue; }

                for (Course course : si.getCourseSchedule().getCourses()) {
                    for (int ii = 0; ii < 3; ++ii) {
                        Session session = new Session(day, si, course);
                        session.setId(id++);

                        logger.debug(session.toString());

                        siSessionList.add(session);
                    }
                }
            }

//            Course course = (Course) si.getCourseSchedule().getCourseMap().values().toArray()[0];
//            Session classSession = new Session(DayOfWeek.MONDAY, si, course);
//            classSession.setClassSessionMeeting(course.getSections().get(0).getClassMeetings().get(0));
//
//            logger.debug("{} {}", classSession.isClassSession(), classSession);

            siList.add(si);
            sessionList.addAll(siSessionList);
//            sessionList.add(classSession);
        }

        //logger.info("Built SI Schedule:\n\t{}\n{}\n\t{}\n", siList, courseSchedule, sessionList);

        SISchedule siSchedule = new SISchedule();
        siSchedule.setSupplementalInstructors(siList);
        siSchedule.setCourseSchedule(courseSchedule);
        siSchedule.setSessions(sessionList);

        return siSchedule;
    }
}
