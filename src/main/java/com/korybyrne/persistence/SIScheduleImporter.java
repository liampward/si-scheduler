package com.korybyrne.persistence;

import com.korybyrne.domain.SISchedule;
import com.korybyrne.domain.Session;
import com.korybyrne.domain.TimeBlock;
import com.korybyrne.domain.course.Course;
import com.korybyrne.domain.course.CourseSchedule;
import com.korybyrne.domain.si.RequestedHours;
import com.korybyrne.domain.si.SupplementalInstructor;
import java.io.FileInputStream;
import java.time.DayOfWeek;
import java.time.Duration;
import java.time.LocalTime;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.List;
import java.util.Set;
import java.util.TreeSet;
import java.util.stream.Collectors;
import javax.json.Json;
import javax.json.JsonArray;
import javax.json.JsonObject;
import javax.json.JsonReader;
import javax.json.JsonString;
import javax.json.JsonValue;

public class SIScheduleImporter {
    public static SISchedule importSavedSchedule(String file, CourseSchedule semesterSchedule) throws Exception{
        FileInputStream inputStream = new FileInputStream(file);
        
        JsonReader reader = Json.createReader(inputStream);
        
        JsonObject schedule = reader.readObject();
        
        JsonArray siArray = schedule.getJsonArray("SIArray");
        
        SISchedule siSchedule = new SISchedule();
        List<SupplementalInstructor> sis = new ArrayList<>();
        List<Session> sessions = new ArrayList<>();
        for(JsonValue si_val : siArray){
            JsonObject si = (JsonObject) si_val;
            
            String name = si.getString("Name");
            int id = si.getInt("ID");
            
            JsonArray requestedCoursesJson = si.getJsonArray("RequestedCourses");
            //do stuff with requested courses
            TreeSet<String> requestedCourses = (TreeSet<String>)jsonArrayToStringSet(requestedCoursesJson);
            
            //for now our "courselist" is just going to be the requested courses
            List<Course> courseList = semesterSchedule.getCourseSubset(new ArrayList<>(requestedCourses));
            
            long maxHours = si.getInt("MaxRequestedHours");
            long minHours = si.getInt("MinRequestedHours");
            
            RequestedHours requestedHours = new RequestedHours(Duration.ofHours(minHours),
                                                               Duration.ofHours(maxHours));
            
            JsonArray busyTimesJson = si.getJsonArray("BusyTimes");
            //do stuff with busytimes
            List<TimeBlock> busyTimes = new ArrayList<>();
            for(JsonValue value : busyTimesJson){
                JsonObject obj = (JsonObject) value;
                
                DayOfWeek day = DayOfWeek.valueOf(obj.getString("Day"));
                
                String startTimeString = obj.getString("StartTime");
                LocalTime startTime = LocalTime.parse(startTimeString);
                
                Duration duration = Duration.ofHours(obj.getInt("Duration"));
                
                busyTimes.add(new TimeBlock(day, startTime, duration));
            }
            
            SupplementalInstructor newSI = new SupplementalInstructor(name, id, requestedHours,
                                                                      busyTimes, courseList, requestedCourses);
            
            JsonArray sessionsJson = si.getJsonArray("Sessions");
            //do stuff with sessions
            for(JsonValue value : sessionsJson){
                JsonObject obj = (JsonObject) value;
                
                DayOfWeek day = DayOfWeek.valueOf(obj.getString("Day"));
                
                String startTimeString = obj.getString("StartTime");
                LocalTime startTime = LocalTime.parse(startTimeString);
                
                Duration duration = Duration.ofHours(obj.getInt("Duration"));
                
                JsonArray coursesJson = obj.getJsonArray("Courses");
                TreeSet<String> coursesString = (TreeSet<String>)jsonArrayToStringSet(coursesJson);
                List<Course> courses = semesterSchedule.getCourseSubset(new ArrayList<>(coursesString));
                
                Session newSession = new Session(day, newSI, courses, startTime, duration);
                sessions.add(newSession);
            }
            
            sis.add(newSI);
        }
        siSchedule.addSIs(sis);
        siSchedule.addSessions(sessions);
        return siSchedule;
    }
    
    private static TreeSet<String> jsonArrayToStringSet(JsonArray jsonArray){
        List<JsonString> jsonString = jsonArray.getValuesAs(JsonString.class);
        return jsonString.stream().map(s -> s.getString()).collect(Collectors.toCollection(TreeSet::new));
    }
}
