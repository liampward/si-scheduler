package com.korybyrne.persistence;

import com.korybyrne.domain.SISchedule;
import com.korybyrne.domain.Session;
import com.korybyrne.domain.course.Course;
import com.korybyrne.domain.course.CourseSchedule;
import java.io.File;

import java.io.IOException;
import java.io.PrintWriter;
import java.time.DayOfWeek;
import java.time.LocalTime;
import java.time.format.DateTimeFormatter;
import java.time.format.FormatStyle;
import java.time.format.TextStyle;
import java.util.Locale;
import org.docx4j.jaxb.Context;
import org.docx4j.openpackaging.exceptions.Docx4JException;
import org.docx4j.openpackaging.exceptions.InvalidFormatException;
import org.docx4j.openpackaging.packages.WordprocessingMLPackage;
import org.docx4j.wml.BooleanDefaultTrue;
import org.docx4j.wml.ObjectFactory;
import org.docx4j.wml.P;
import org.docx4j.wml.R;
import org.docx4j.wml.RPr;
import org.docx4j.wml.Text;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class SIScheduleExporter {
    private static final Logger logger = LoggerFactory.getLogger(SIAvailabilityImporter.class);
    
    private static final int csvWidth = 12;
    private static final int csvHeight = 17;

    private static final Table.Position sessionsStart = new Table.Position(1, 1);
    private static final Table.Position sessionsEnd   = new Table.Position(16, 7);

    private static final Table.Position requestedMinimumHours = new Table.Position(1,10);
    private static final Table.Position requestedMaximumHours = new Table.Position(2,10);

    private static final Table.Position requestedCoursesStart = new Table.Position(5, 10);
    private static final Table.Position requestedCoursesEnd = new Table.Position(16, 10);

    private static final Table.Position inClassStart = new Table.Position(5, 11);
    private static final Table.Position inClassEnd   = new Table.Position(16, 10);

    private static final Table.Position namePosition = new Table.Position(0, 10);
    private static final Table.Position idPosition   = new Table.Position(3, 10);

    private static final int startHour = 6;
    
    public void exportCsvSchedule(SISchedule siSchedule, String dest) throws IOException{
        PrintWriter writer = new PrintWriter(dest);
        String[][] rows = buildCsvScheduleStringArray();
        
        for(int i = 0; i < csvHeight; ++i){
            for(int j = 0; j < csvWidth; ++j){
                System.out.println(rows[i][j]);
                writer.write(rows[i][j]);
            }
            writer.write("\n");
        }
        siSchedule.getSupplementalInstructors();
        
        writer.flush();
    }
    
    public void exportDocxFile(SISchedule siSchedule,CourseSchedule courseSchedule,
            String dest) throws IOException, InvalidFormatException, Docx4JException{
        WordprocessingMLPackage wordMLPackage = WordprocessingMLPackage.createPackage();
        ObjectFactory factory = Context.getWmlObjectFactory();
        
        //docx4j has the format:
        //Paragraph(P) -> Run(R)-> Text
        //So respectively we'll have:
        //Divider(P), and then X runs that hold Course and Session info as Text
        for(Course course : courseSchedule.getCourses()){
            boolean firstTimeThrough = true;
            
            for(int i = 1; i < 8; ++i){
                int day = (i == 1) ? 7 : i - 1;
                DayOfWeek currentDay = DayOfWeek.of(day);
                for(Session session : siSchedule.getSessions()){
                    if(firstTimeThrough){
                        P header = factory.createP();

                        R courseInfoRun = factory.createR();
                        Text courseInfoText = factory.createText();
                        courseInfoText.setValue(course.toString());
                        RPr runProperties = factory.createRPr();
                        BooleanDefaultTrue b = new BooleanDefaultTrue();
                        runProperties.setB(b);
                        courseInfoRun.getContent().add(courseInfoText);
                        courseInfoRun.setRPr(runProperties);

                        header.getContent().add(courseInfoRun);
                        wordMLPackage.getMainDocumentPart().getContent().add(header);
                        firstTimeThrough = false;
                    }
                    if(session.getCourses().contains(course) && session.getDay() == currentDay){
                        P paragraph = factory.createP();
                        R sessionInfoRun = factory.createR();
                        Text sessionInfoText = factory.createText();
                        sessionInfoText.setValue(session.timesToString());
                        sessionInfoRun.getContent().add(sessionInfoText);

                        paragraph.getContent().add(sessionInfoRun);

                        wordMLPackage.getMainDocumentPart().getContent().add(paragraph);
                    }
                }
            }
        }
        wordMLPackage.save(new File(dest));
    }
    
    public void exportJsonSchedule(SISchedule siSchedule, String dest) throws Exception{
        PrintWriter writer = new PrintWriter(dest);
        writer.write(siSchedule.toJson().toString());
        writer.flush();
        writer.close();
    }
    
    public String[][] buildCsvScheduleStringArray(){
        //This way all the unchanged blocks will be commas
        String[][] rows = initializeCsvArray();
        
        //Row labels
        LocalTime hour = LocalTime.of(6, 0);
        for(int i = 1; i < csvHeight; ++i){
            rows[i][0] = hour.format(DateTimeFormatter.ofLocalizedTime(FormatStyle.SHORT)) + ",";
            hour = hour.plusHours(1);
        }
        
        //Column labels
        for(int i = 1; i < 8; ++i){
            int day = (i == 1) ? 7 : i - 1; //Java's week starts on Monday, ours starts on Sunday
            rows[0][i] = DayOfWeek.of(day).getDisplayName(TextStyle.FULL, Locale.US) + ",";
        }
        
        return rows;
    }
    
    private String[][] initializeCsvArray(){
        String[][] rows = new String[csvHeight][csvWidth];
        for(int i = 0; i < csvHeight; ++i){
            for(int j = 0; j < csvWidth; ++j){
                rows[i][j] = ",";
            }
        }
        return rows;
    }
}
