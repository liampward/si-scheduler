package com.korybyrne.swinggui;

import com.korybyrne.domain.SISchedule;
import com.korybyrne.domain.course.CourseSchedule;
import com.korybyrne.persistence.SIScheduleExporter;
import com.korybyrne.persistence.SIScheduleGenerator;
import com.korybyrne.persistence.SIScheduleImporter;
import com.korybyrne.persistence.course.CourseScheduleImporter;
import com.korybyrne.swinggui.editsi.AddSessionGUI;
import com.korybyrne.swinggui.configuration.ConfigurationPanel;
import com.korybyrne.swinggui.details.DetailsPane;
import com.korybyrne.swinggui.instructors.InstructorsPane;
import com.korybyrne.swinggui.schedule.SchedulePanel;
import com.korybyrne.swinggui.selector.SelectorPane;
import org.optaplanner.core.api.solver.Solver;
import org.optaplanner.core.api.solver.SolverFactory;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.File;
import java.io.IOException;
import java.text.DecimalFormat;
import java.text.NumberFormat;
import java.util.concurrent.ExecutionException;
import javax.swing.filechooser.FileNameExtensionFilter;
import javax.xml.parsers.ParserConfigurationException;
import org.xml.sax.SAXException;

public class ScheduleMakerGUI implements ActionListener {
    public static NumberFormat decimalFormatter = new DecimalFormat("##.#");

    private Logger logger = LoggerFactory.getLogger(getClass());

    private JFrame window;

    private SchedulePanel schedulePanel;
    private ConfigurationPanel configurationPanel;
    private SelectorPane selectorPane;
    private DetailsPane detailsPane;
    private InstructorsPane instructorsPane;

    private JMenuBar menuBar;
    private JMenu fileMenu;
    private JMenu importMenu;
    private JMenu exportMenu;
    private JMenuItem importSIMenuItemCsv;
    private JMenuItem importSIMenuItemJson;
    //private JMenuItem exportScheduleMenuItemDocx;
    private JMenuItem exportScheduleMenuItemJson;
    private JMenuItem lockAllSelectedSIsMenuItem;
    private JMenuItem addSessionMenuItem;
    private JMenuItem generateScheduleMenuItem;

    private ScheduleMakerController controller;

    public ScheduleMakerGUI() throws IOException, SAXException, ParserConfigurationException {

        schedulePanel = new SchedulePanel();
        selectorPane = new SelectorPane();
        detailsPane = new DetailsPane();
        instructorsPane = new InstructorsPane();
        configurationPanel = new ConfigurationPanel();
        controller = new ScheduleMakerController(schedulePanel, detailsPane, instructorsPane);
        controller.observeScheduleComponents(schedulePanel, configurationPanel, selectorPane, detailsPane, instructorsPane);
        
        
        CourseSchedule courseSchedule = CourseScheduleImporter.importFromResource("/com/korybyrne/persistence/courses.xml");
        
        controller.setSchedule(new SISchedule());
        controller.getSchedule().setCourseSchedule(courseSchedule);

        window = new JFrame("SI Schedule Maker");
        window.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        window.setLayout(new BorderLayout());
        window.setResizable(true);

        this.addMenuComponents();
        this.addUIComponents();

        window.setPreferredSize(new Dimension(1280, 720));
        window.pack();
        window.setLocationRelativeTo(null);
        window.setVisible(true);

        detailsPane.setPreferredSize(detailsPane.getMinimumSize());
    }

    private void addMenuComponents() {
        menuBar = new JMenuBar();
            fileMenu = new JMenu("File");
                importMenu = new JMenu("Import");
                    importSIMenuItemCsv = new JMenuItem("Import SI Spreadsheets");
                        importSIMenuItemCsv.addActionListener(this);
                    importSIMenuItemJson = new JMenuItem("Import Full Schedule");
                        importSIMenuItemJson.addActionListener(this);
                    importMenu.add(importSIMenuItemCsv);
                    importMenu.add(importSIMenuItemJson);
                    
                exportMenu = new JMenu("Export");
                    //exportScheduleMenuItemDocx = new JMenuItem("Export Schedule as .docx");
                        //exportScheduleMenuItemDocx.addActionListener(this);
                    //exportMenu.add(exportScheduleMenuItemDocx);
                    exportScheduleMenuItemJson = new JMenuItem("Save Schedule");
                        exportScheduleMenuItemJson.addActionListener(this);
                    exportMenu.add(exportScheduleMenuItemJson);
                
                    
                lockAllSelectedSIsMenuItem = new JMenuItem("Lock Selected SIs");
                    lockAllSelectedSIsMenuItem.addActionListener(this);
                generateScheduleMenuItem = new JMenuItem("Generate Schedule");
                    generateScheduleMenuItem.addActionListener(this);
                
                fileMenu.add(importMenu);
                fileMenu.add(exportMenu);
                fileMenu.add(lockAllSelectedSIsMenuItem);
                fileMenu.add(generateScheduleMenuItem);

            menuBar.add(fileMenu);
        window.setJMenuBar(menuBar);
    }

    private void addUIComponents() {
        Container content = window.getContentPane();

//        JSplitPane selectorScheduleSplit = new JSplitPane(JSplitPane.HORIZONTAL_SPLIT, selectorPane, schedulePanel);
//        JSplitPane allMiddleSplit = new JSplitPane(JSplitPane.HORIZONTAL_SPLIT, selectorScheduleSplit, detailsPane);
//
//        JSplitPane configMiddleSplit = new JSplitPane(JSplitPane.VERTICAL_SPLIT, configurationPanel, allMiddleSplit);
//        JSplitPane allSplit = new JSplitPane(JSplitPane.VERTICAL_SPLIT, configMiddleSplit, instructorsPane);
//
//        content.add(BorderLayout.CENTER, allSplit);

        content.add(BorderLayout.PAGE_START, configurationPanel);
        content.add(BorderLayout.LINE_START, selectorPane);
        content.add(BorderLayout.CENTER, schedulePanel);
        content.add(BorderLayout.LINE_END, detailsPane);
        content.add(BorderLayout.PAGE_END, instructorsPane);
    }

    @Override
    public void actionPerformed(ActionEvent actionEvent) {
        Object source = actionEvent.getSource();

        if (source == generateScheduleMenuItem) {
            if(controller.getSchedule().getSupplementalInstructors().isEmpty()){
                JOptionPane.showMessageDialog(null, "No SIs have been imported!");
                return;
            }
            new ScheduleGenerationTask().execute();
        } else if(source == importSIMenuItemCsv){
            new SIImporterTask().execute();
        } else if(source == importSIMenuItemJson){
            new ScheduleImporterTask().execute();
        //} else if(source == exportScheduleMenuItemDocx){
           // new SIExporterTaskDocx().execute();
        } else if(source == exportScheduleMenuItemJson){
            new SIExporterTaskJson().execute();
        } else if(source == lockAllSelectedSIsMenuItem){
            controller.lockSelectedSISessions();
        }
    }
    
    private class SIImporterTask extends SwingWorker<SISchedule, Void>{

        @Override
        protected SISchedule doInBackground() throws Exception {

            JFileChooser fileChooser = new JFileChooser();
            FileNameExtensionFilter filter = new FileNameExtensionFilter("SI Availability File", "csv");
            File[] selectedFiles;

            fileChooser.setFileFilter(filter);
            fileChooser.setMultiSelectionEnabled(true);
            
            switch(fileChooser.showOpenDialog(window)){
                case JFileChooser.APPROVE_OPTION:
                    selectedFiles = fileChooser.getSelectedFiles();
                    
                    return new SIScheduleGenerator().generateSchedule(
                        "/com/korybyrne/persistence/courses.xml", selectedFiles, selectedFiles.length);
            }
            return null;
        }
        
        @Override
        protected void done(){
            try{
                if(controller.getSchedule() == null)
                    controller.setSchedule(get());
                else{
                    controller.addToSchedule(get());
                }
            }catch(InterruptedException | ExecutionException ignore){
                //Actually handle this?
                System.out.println(ignore);
            }catch(NullPointerException e){
            }
        }
    }
    
    private class ScheduleImporterTask extends SwingWorker<SISchedule, Void>{

        @Override
        protected SISchedule doInBackground() throws Exception {
            JFileChooser fileChooser = new JFileChooser();
            FileNameExtensionFilter filter = new FileNameExtensionFilter("SI Availability File", "json");
            
            fileChooser.setFileFilter(filter);
            
            switch(fileChooser.showOpenDialog(window)){
                case JFileChooser.APPROVE_OPTION:
                    File file = fileChooser.getSelectedFile();
                    
                     return SIScheduleImporter.importSavedSchedule(file.getAbsolutePath(), 
                                        controller.getSchedule().getCourseSchedule());
            }
            return null;
        }
        
        @Override
        protected void done(){
            try{
                if(controller.getSchedule() == null)
                    controller.setSchedule(get());
                else{
                    controller.addToSchedule(get());
                }
            }catch(InterruptedException | ExecutionException ignore){
                //Actually handle this?
                System.out.println(ignore);
            }catch(NullPointerException e){
            }
        }
    }
    
    private class ScheduleGenerationTask extends SwingWorker<SISchedule, Void> {
        @Override
        public SISchedule doInBackground() {
            SISchedule unfinishedSchedule = controller.getSchedule();

            SolverFactory<SISchedule> siScheduleSolverFactory = SolverFactory.createFromXmlResource(
                    "com/korybyrne/solver/siSchedulingSolverConfig.xml"
            );
            
            Solver<SISchedule> siScheduleSolver = siScheduleSolverFactory.buildSolver();

            siScheduleSolver.solve(unfinishedSchedule);
            SISchedule bestSchedule = siScheduleSolver.getBestSolution();
            //bestSchedule.removeEmptySessions();
            
            return bestSchedule;
        }

        @Override
        protected void done() {
            try {
                logger.info("Almost done...");
                controller.setSchedule(get());
                logger.info("Done, repainting.");
                configurationPanel.repaint();
                selectorPane.repaint();
            } catch (InterruptedException | ExecutionException ignore) {
                //Actually handle this at some point?
                System.out.println(ignore);
            }
        }
    }
    
    private class SIExporterTaskDocx extends SwingWorker<SISchedule, Void>{
        @Override
        protected SISchedule doInBackground() throws Exception {
            SIScheduleExporter exporter = new SIScheduleExporter();
            exporter.exportDocxFile(controller.getSchedule(), 
                    controller.getSchedule().getCourseSchedule(),"test.docx");
            return null;
        }
    }
    
    private class SIExporterTaskJson extends SwingWorker<SISchedule, Void>{

        @Override
        protected SISchedule doInBackground() throws Exception {
            SIScheduleExporter exporter = new SIScheduleExporter();
            
            JFileChooser chooser = new JFileChooser();
            
            if(chooser.showSaveDialog(null) == JFileChooser.APPROVE_OPTION){
                exporter.exportJsonSchedule(controller.getSchedule(), chooser.getSelectedFile() + ".json");
            }
            return null;
        }
        
        @Override
        protected void done(){
            try{
                get();
            } catch(Exception e){
                e.printStackTrace();
            }
        }
    }
}
