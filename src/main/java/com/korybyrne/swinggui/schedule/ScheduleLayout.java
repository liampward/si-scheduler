package com.korybyrne.swinggui.schedule;

import com.korybyrne.domain.TimeBlock;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.swing.*;
import java.awt.*;
import java.time.DayOfWeek;
import java.time.Duration;
import java.time.LocalTime;
import java.time.format.TextStyle;
import java.util.HashMap;
import java.util.Locale;
import java.util.Map;

public class ScheduleLayout implements LayoutManager2 {
    private Logger logger = LoggerFactory.getLogger(getClass());

    private JLayeredPane parent;

    private Map<Component, TimeBlock> componentTimeBlocks;
    private DayOfWeek startDay, endDay;
    private LocalTime startTime, endTime;

    private int numDays;
    private Duration displayDuration;
    private double displayDurationSeconds;

    public ScheduleLayout(DayOfWeek startDay, DayOfWeek endDay, LocalTime startTime, LocalTime endTime, JLayeredPane layeredPane) {
        this.componentTimeBlocks = new HashMap<>();
        this.startDay = startDay;
        this.endDay = endDay;
        this.startTime = startTime;
        this.endTime = endTime;

        this.numDays = this.endDay.getValue() - this.startDay.getValue() + 1;
        this.displayDuration = Duration.between(this.startTime, this.endTime);
        this.displayDurationSeconds = this.displayDuration.getSeconds();

        this.parent = layeredPane;
    }

    @Override
    public void addLayoutComponent(Component component, Object constraints) {
        if (! (constraints instanceof TimeBlock)) {
            throw new IllegalArgumentException("Constraints object must be of type <TimeBlock>!");
        }

        TimeBlock timeBlock = (TimeBlock) constraints;
        componentTimeBlocks.put(component, timeBlock);
    }

    @Override
    public Dimension maximumLayoutSize(Container container) {
        return container.getSize();
    }

    @Override
    public float getLayoutAlignmentX(Container container) {
        return SwingConstants.CENTER;
    }

    @Override
    public float getLayoutAlignmentY(Container container) {
        return SwingConstants.CENTER;
    }

    @Override
    public void invalidateLayout(Container container) {

    }

    @Override
    public void addLayoutComponent(String s, Component component) {

    }

    @Override
    public void removeLayoutComponent(Component component) {
        componentTimeBlocks.remove(component);
    }

    @Override
    public Dimension preferredLayoutSize(Container container) {
        return container.getSize();
    }

    @Override
    public Dimension minimumLayoutSize(Container container) {
        return new Dimension(0, 0);
    }

    @Override
    public void layoutContainer(Container container) {
        Dimension size = container.getSize();
        Insets insets = container.getInsets();

        double width = size.getWidth() + insets.left + insets.right;
        double height = size.getHeight() + insets.top + insets.bottom;

        double componentWidth = width / numDays;
        int numComponents = container.getComponentCount();

        logger.info("{} {} {} {}", width, height, componentWidth, numComponents);

        for (int i = 0; i < numComponents; ++i) {
            Component component = container.getComponent(i);
            TimeBlock timeBlock = componentTimeBlocks.get(component);

            if (timeBlock == null) {
                throw new IllegalArgumentException("Components must be added with a TimeBlock constraint!");
            }

            double day = timeBlock.getDay().getValue() - startDay.getValue();

            if (day < 0) {
                logger.trace(
                        "Constraint {} day must be between: {} and {}; skipping",
                        timeBlock,
                        startDay.getDisplayName(TextStyle.FULL, Locale.US),
                        endDay.getDisplayName(TextStyle.FULL, Locale.US));

                continue;
            }

            double normalizedStart = Duration.between(startTime, timeBlock.getStartTime()).getSeconds();

            double x = day * componentWidth;
            double y = (normalizedStart / displayDurationSeconds) * height;
            double h = (((double) timeBlock.getDuration().getSeconds()) / displayDurationSeconds) * height;

            component.setBounds((int) x, (int) y, (int) componentWidth, (int) h);
        }
    }
}
