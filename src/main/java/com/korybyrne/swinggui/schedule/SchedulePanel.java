package com.korybyrne.swinggui.schedule;

import com.korybyrne.domain.Session;
import com.korybyrne.domain.TimeBlock;
import com.korybyrne.domain.course.ClassMeeting;
import com.korybyrne.domain.course.Course;
import com.korybyrne.domain.course.CourseSchedule;
import com.korybyrne.domain.course.Section;
import com.korybyrne.domain.si.SupplementalInstructor;
//import com.korybyrne.swinggui.DragListener;
import com.korybyrne.swinggui.ScheduleComponent;
import com.korybyrne.swinggui.ScheduleMakerController;
import org.apache.commons.lang3.builder.CompareToBuilder;
import org.jetbrains.annotations.NotNull;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.swing.*;
import java.awt.*;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.io.IOException;
import java.time.DayOfWeek;
import java.time.Duration;
import java.time.LocalTime;
import java.util.Collection;
import java.util.Random;
import java.util.SortedSet;
import java.util.TreeSet;
import java.util.logging.Level;
import javax.swing.event.MouseInputAdapter;

public class SchedulePanel extends JLayeredPane implements ScheduleComponent {
    private static final Color busyColor = new Color(190, 220, 213);

    private static final Logger logger = LoggerFactory.getLogger(SchedulePanel.class);

    private ScheduleMakerController controller;
    private LayoutManager scheduleLayout;
    
    private final int hours = 12;
    private final int days = 6;

    public SchedulePanel() {
        this.scheduleLayout = new ScheduleLayout(
                DayOfWeek.MONDAY, DayOfWeek.SATURDAY,
                LocalTime.of(9, 00), LocalTime.of(12 + 9, 00),
                this
        );

        this.setLayout(this.scheduleLayout);
    }

    @Override
    public void onScheduleChanged() {
        this.removeAll();

        Random rand = new Random();
        // sorted order to prevent overlaps
        SortedSet<ComponentTimeBlock> components = new TreeSet<>();
        for (SupplementalInstructor supplementalInstructor : controller.getActiveSIs()) {
            if (controller.isShowingClassMeetings()) {
                CourseSchedule courseSchedule = supplementalInstructor.getCourseSchedule();
                Collection<Course> courseCollection = 
                        controller.getActiveCourses().isEmpty() ? courseSchedule.getCourses()
                                                                : controller.getActiveCourses();
                for (Course course : courseCollection) {
                    //if course not a selectec course, continue
                    //if no courses are active, show all info
                    for (Section section : course.getSections()) {
                        for (ClassMeeting meeting : section.getClassMeetings()) {
                            JPanel meetingPanel = makeClassMeetingPanel(course, section, meeting);
                            components.add(new ComponentTimeBlock(meetingPanel, meeting, 1));
                        }
                    }
                }
            }

            if (controller.isShowingBusyTimes()) {
                for (TimeBlock busyTime : supplementalInstructor.getBusyTimes()) {
                    JPanel busyPanel = makeBusyPanel(busyTime);
                    components.add(new ComponentTimeBlock(busyPanel, busyTime, 0));
                }
            }
        }

        if (controller.isShowingSessions()) {
            for (Session session : controller.getSchedule().getSessions()) {
                if (controller.isActiveSI(session.getSupplementalInstructor()) &&
                    (controller.getActiveCourses().contains(session.getCoursesAsList().get(0)) ||
                     controller.getActiveCourses().isEmpty())) {
                    
                    JPanel sessionPanel = makeSessionPanel(session);
                    components.add(new ComponentTimeBlock(sessionPanel, session, 2));
                }
            }
        }

        int i = 0;
        for (ComponentTimeBlock ctb : components) {
            this.add(ctb.component, ctb.timeBlock);
            this.setComponentZOrder(ctb.component, i++);
        }

        this.revalidate();
        this.repaint();
    }

    //////////////////
    // Drawing
    //////////////////

    @Override
    protected void paintComponent(Graphics g) {
        int w = getWidth();
        int h = getHeight();

        Graphics2D g2d = (Graphics2D) g;

        g2d.setColor(Color.cyan);
        g2d.fillRect(0, 0, w, h);

        g2d.setColor(Color.black);
        for (float hour = 0; hour <= hours; hour++) {
            int y = (int) ((hour / hours) * h);

            g2d.drawLine(0, y, w, y);
        }

        for (float day = 0; day <= days; day++) {
            int x = (int) ((day / days) * w);

            g2d.drawLine(x, 0, x, h);
        }
    }

    private static class ComponentTimeBlock implements Comparable<ComponentTimeBlock> {
        private static long ID = 0;

        private Component component;
        private TimeBlock timeBlock;
        private int precedence;
        private long id = ID++;

        public ComponentTimeBlock(Component component, TimeBlock timeBlock, int precedence) {
            this.component = component;
            this.timeBlock = timeBlock;
            this.precedence = precedence;
        }

        @Override
        public int compareTo(@NotNull ComponentTimeBlock rhs) {
            return - new CompareToBuilder()
                    .append(timeBlock.getStartTime(), rhs.timeBlock.getStartTime())
                    .append(precedence, rhs.precedence)
                    .append(id, rhs.id)
                    .toComparison();
        }
    }

    private abstract class BlockMouseListener implements MouseListener {
        private final Color baseColor;
        private final Component component;

        BlockMouseListener(Component component, Color baseColor) {
            this.component = component;
            this.baseColor = baseColor;
        }

        @Override
        public abstract void mousePressed(MouseEvent mouseEvent);
        
        @Override
        public void mouseClicked(MouseEvent mouseEvent){
            
        }

        @Override
        public void mouseReleased(MouseEvent mouseEvent) {

        }

        @Override
        public void mouseEntered(MouseEvent mouseEvent) {
            changeComponentColor(baseColor.darker());
        }

        @Override
        public void mouseExited(MouseEvent mouseEvent) {
            changeComponentColor(baseColor);
        }

        protected void changeComponentColor(Color newColor) {
            this.component.setBackground(newColor);
            repaint();
        }
    }
    private class DragListener extends MouseInputAdapter {

        Point location;
        MouseEvent pressed;
        
        boolean dragged = false;

        int dayChange;
        float hourChange;
        int halfHourChange;
        
        @Override
        public void mousePressed(MouseEvent event){
            pressed = event;
        }
        
        @Override
        public void mouseReleased(MouseEvent event){
            if(dragged == false || event.getButton() == MouseEvent.BUTTON2)
                return;
            
            TimeBlock curTimeBlock = controller.getCurrentTimeBlock();
            
            if(event.getButton() == MouseEvent.BUTTON1){
                curTimeBlock.setDay(DayOfWeek.of(dayChange));

                curTimeBlock.setStartTime(LocalTime.of((int)hourChange, halfHourChange));

                if(curTimeBlock.getEndTime().compareTo(LocalTime.of(21, 0)) > 0 ||
                        curTimeBlock.getEndTime()
                        .compareTo(curTimeBlock.getStartTime()) < 0){

                    curTimeBlock.setEndTime(LocalTime.of(21, 0));
                }
            } else if(event.getButton() == MouseEvent.BUTTON3){
                curTimeBlock.setEndTime(LocalTime.of((int)hourChange, halfHourChange));
                
                if(curTimeBlock.getDuration().isNegative() || curTimeBlock.getDuration().isZero()){
                    curTimeBlock.setEndTime(curTimeBlock.getStartTime().plusMinutes(30));
                }
            }
            
            dragged = false;
            controller.notifyScheduleChange();
        }

        @Override
        public void mouseDragged(MouseEvent event){
            dragged = true;
            if(pressed.getButton() == MouseEvent.BUTTON1){
                move(event);
            } else if(pressed.getButton() == MouseEvent.BUTTON3){
                resize(event);
            }
        }
        
        private void move(MouseEvent event){
            Component comp = event.getComponent();
            int h = getHeight();
            int w = getWidth();
            int days = getDaysDivider();
            int halfhours = getHoursDivider() * 2;

            location = comp.getLocation(location);
            int x = location.x + event.getX();
            int y = (location.y + event.getY()) - (comp.getHeight() / 2);
            //snap to nearest day and half hour here

            int day = Math.round(x / (w / days)) ;
            if(day < 0)
                day = 0;
            else if(day > 5)
                day = 5;
            int nearestX = day * (w / days);
            
            //This doesn't quite line up, and I can't seem to figure out why.
            int hour = Math.round(y / (h / halfhours));
            double end = controller.getCurrentTimeBlock().getDuration().toMinutes();
            end = (end / 60) * 2;

            if(hour < 0)
                hour = 0;
            else if(hour + end > 24)
                hour = (int) (24 - (end));
            
            dayChange = day + 1;
            hourChange  = (hour / 2) + 9;
            halfHourChange = (hour % 2 == 0) ? 0 : 30;
            
            //I stole this from ScheduleLayout I'm not actually sure why it works
            //but I assume it has to do with the SchedulePanel's height being larger than what is shown.
            double normalizedStart = Duration.between(LocalTime.of(9, 0),LocalTime.of((int)hourChange, halfHourChange))
                    .getSeconds();
            Duration fullDay = Duration.between(LocalTime.of(9,0), LocalTime.of(9 + 12, 0));
            double nearestY = (normalizedStart / fullDay.getSeconds()) * h;
            
            //For now we'll just use a sliding y, it looks fine and is intuitive enough I think.
            comp.setLocation(nearestX, (int) nearestY);
        }
        
        private void resize(MouseEvent event){
            Component comp = event.getComponent();
            Container parent = comp.getParent();
            int h = getHeight();
            int halfhours = getHoursDivider() * 2;
            
            location = comp.getLocation();
            int y = location.y + event.getY();
            
            int hour = Math.round(y / (h / halfhours));
            int startHour = Math.round(location.y / (h / halfhours));
            
            if(hour <= startHour)
                hour = startHour + 1;
            else if(hour > 24)
                hour = 24;
            
            hourChange  = (hour / 2) + 9;
            halfHourChange = (hour % 2 == 0) ? 0 : 30;
            
            double normalizedStart = Duration.between(LocalTime.of(9, 0),LocalTime.of((int)hourChange, halfHourChange))
                    .getSeconds();
            double fullDay = Duration.between(LocalTime.of(9,0), LocalTime.of(9 + 12, 0)).getSeconds();
            double partOfDay = normalizedStart / fullDay;
            double nearestY = partOfDay * h;
            
            comp.setSize(comp.getWidth(), (int) (nearestY - location.y));
        }
    }

    private JPanel makeSessionPanel(Session session) {
        JPanel sessionPanel = new JPanel(new BorderLayout());
        JPanel coursePanel = new JPanel(new FlowLayout(FlowLayout.CENTER));
        
        int green = session.getCoursesAsList().get(0).getCourseCode().hashCode();
        green = green % 100;
        green = (green < 0) ? green + 255 : green + 155;
        
        int red = session.getCoursesAsList().get(0).getCodeNumber().hashCode();
        red = red % 100;
        red = (red < 0) ? red + 50 : red + 25;
        
        int blue = session.getCoursesAsList().get(0).getDepartment().hashCode();
        blue = blue % 100;
        blue = (blue < 0) ? blue + 50 : blue + 25;

        Color color = new Color(red, green, blue);

        for (Course course : session.getCourses()) {
            JLabel courseLabel = new JLabel(course.getCourseCode());

            coursePanel.add(courseLabel);
        }
        JLabel siLabel = new JLabel(session.getSupplementalInstructor().getName());

        sessionPanel.add(BorderLayout.PAGE_START, siLabel);
        sessionPanel.add(BorderLayout.CENTER, coursePanel);
        
        setPanelLooks(sessionPanel, color);
        coursePanel.setBackground(color);

        sessionPanel.addMouseListener(new BlockMouseListener(sessionPanel, color) {
            @Override
            public void mousePressed(MouseEvent mouseEvent) {
                controller.onSessionClicked(session);
            }
        });
        
        DragListener drag = new DragListener();
        sessionPanel.addMouseListener(drag);
        sessionPanel.addMouseMotionListener(drag);
        
        return sessionPanel;
    }

    private JPanel makeClassMeetingPanel(Course course, Section section, ClassMeeting classMeeting) {
        JPanel classMeetingPanel = new JPanel(new BorderLayout());
        JLabel classMeetingLabel = new JLabel(classMeeting.toString());

        int red = course.getCourseCode().hashCode();
        red = red % 50;
        red += 205;
        
        int green = course.getCodeNumber().hashCode();
        green = green % 50;
        green = (green < 0) ? green + 50 : green + 25;
        
        int blue = course.getDepartment().hashCode();
        blue = blue % 20;
        blue = (blue < 0) ? blue + 20 : blue + 10;

        Color color = new Color(red, green, blue);
        
        classMeetingPanel.add(BorderLayout.PAGE_START, classMeetingLabel);
        setPanelLooks(classMeetingPanel, color);

        classMeetingPanel.addMouseListener(new BlockMouseListener(classMeetingPanel, color) {
            @Override
            public void mousePressed(MouseEvent mouseEvent) {
                controller.onClassClicked(course, section, classMeeting);
            }

        });

        return classMeetingPanel;
    }

    private JPanel makeBusyPanel(TimeBlock timeBlock) {
        JPanel busyPanel = new JPanel();
        JLabel busyLabel = new JLabel("BUSY");

        busyPanel.add(busyLabel);

        setPanelLooks(busyPanel, busyColor);

        busyPanel.addMouseListener(new BlockMouseListener(busyPanel, busyColor) {
            @Override
            public void mousePressed(MouseEvent mouseEvent) {
                controller.onBusyClicked(timeBlock);
            }
        });
        DragListener drag = new DragListener();
        busyPanel.addMouseListener(drag);
        busyPanel.addMouseMotionListener(drag);
        return busyPanel;
    }

    private static void setPanelLooks(JPanel panel, Color bgColor) {
        panel.setBackground(bgColor);
        panel.setBorder(
                BorderFactory.createLineBorder(Color.BLACK, 1, true)
        );
    }

    @Override
    public void setController(ScheduleMakerController controller) {
        this.controller = controller;
    }
    
    public ScheduleMakerController getController(){
        return this.controller;
    }
    
    public int getDaysDivider(){
        return this.days;
    }
    public int getHoursDivider(){
        return this.hours;
    }
}
