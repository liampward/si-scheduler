package com.korybyrne.swinggui;

import com.korybyrne.domain.SISchedule;
import com.korybyrne.domain.Session;
import com.korybyrne.domain.TimeBlock;
import com.korybyrne.domain.course.ClassMeeting;
import com.korybyrne.domain.course.Course;
import com.korybyrne.domain.course.Section;
import com.korybyrne.domain.si.SupplementalInstructor;
import com.korybyrne.swinggui.details.DetailsPane;
import com.korybyrne.swinggui.instructors.InstructorsPane;
import com.korybyrne.swinggui.schedule.SchedulePanel;
import java.awt.Component;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.time.DayOfWeek;
import java.time.LocalTime;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

public class ScheduleMakerController {
    private Logger logger = LoggerFactory.getLogger(getClass());

    private SISchedule schedule;
    private Collection<ScheduleComponent> observedScheduleComponents;

    private SchedulePanel schedulePanel;
    private DetailsPane detailsPane;
    private InstructorsPane instructorsPane;

    private boolean showingBusyTimes, showingClassMeetings, showingSessions;
    private DetailsPane.Type currentDetails;
    private Course currentCourse;
    private Section currentSection;
    private TimeBlock currentTimeBlock;
    private Session currentSession;

    private List<SupplementalInstructor> activeSIs;
    private List<Course> activeCourses;
    
    public ScheduleMakerController(SchedulePanel schedulePanel, DetailsPane detailsPane, InstructorsPane instructorsPane) {
        showingBusyTimes = showingClassMeetings = showingSessions = true;

        this.schedulePanel = schedulePanel;
        this.detailsPane = detailsPane;
        this.instructorsPane = instructorsPane;

        this.activeSIs = new ArrayList<>();
        this.activeCourses = new ArrayList<>();
        this.observedScheduleComponents = new ArrayList<>();
        this.schedule = null;//new SISchedule();

        currentDetails = DetailsPane.Type.None;
        currentTimeBlock = null;
        currentSession = null;
    }

    ///////////////
    // Active SIs
    ///////////////

    public void clearActiveSIs() {
        activeSIs.clear();
        repaintView();
    }

    public void addActiveSI(SupplementalInstructor si) {
        activeSIs.add(si);
        repaintView();
    }

    public void addActiveSIs(Collection<SupplementalInstructor> siCollection) {
        activeSIs.addAll(siCollection);
        repaintView();
    }

    public void setActiveSIs(Collection<SupplementalInstructor> siCollection) {
        clearActiveSIs();
        addActiveSIs(siCollection);
    }
    
    public void clearActiveCourses() {
        activeCourses.clear();
        repaintView();
    }
    
    //////////////////
    // Active Courses
    //////////////////
    
    public List<Course> getActiveCourses(){
        return this.activeCourses;
    }
    
    public void addActiveCourse(Course course){
        activeCourses.add(course);
        repaintView();
    }
    
    public void addActiveCourses(Collection<Course> courseCollection){
        activeCourses.addAll(courseCollection);
        repaintView();
    }
    
    public void setActiveCourses(Collection<Course> courseCollection){
        clearActiveCourses();
        addActiveCourses(courseCollection);
    }
    
    ///////////////////////
    // Controlling Details
    ///////////////////////

    public void onNoneClicked() {
        this.currentDetails = DetailsPane.Type.None;
        this.currentCourse = null;
        this.currentSection = null;
        this.currentSession = null;
        this.currentTimeBlock = null;

        showDetails();
    }

    public void onBusyClicked(TimeBlock busyTimeBlock) {
        this.currentDetails = DetailsPane.Type.Busy;
        this.currentCourse = null;
        this.currentSection = null;
        this.currentTimeBlock = busyTimeBlock;
        this.currentSession = null;

        showDetails();
    }

    public void onClassClicked(Course course, Section section, ClassMeeting meeting) {
        this.currentDetails = DetailsPane.Type.Class;
        this.currentCourse = course;
        this.currentSection = section;
        this.currentTimeBlock = meeting;
        this.currentSession = null;

        showDetails();
    }

    public void onSessionClicked(Session session) {
        this.currentDetails = DetailsPane.Type.Session;
        this.currentCourse = null;
        this.currentSection = null;
        this.currentTimeBlock = session;
        this.currentSession = session;

        showDetails();
    }

    private void showDetails() {
        switch (this.currentDetails) {
            case None:
                detailsPane.showNoneDetails();
                break;

            case Busy:
                detailsPane.showBusyDetails(currentTimeBlock);
                break;

            case Class:
                detailsPane.showClassPanel(currentCourse, currentSection, (ClassMeeting) currentTimeBlock);
                break;

            case Session:
                detailsPane.showSessionPanel(currentSession);
                break;
        }
    }

    public void setStartTime(LocalTime startTime) {
        if (this.currentTimeBlock == null)
            throw new IllegalStateException("CurrentBusyTime is null!");

        LocalTime endTime = currentTimeBlock.getEndTime();

        if (startTime.isBefore(endTime)) {
            this.currentTimeBlock.setStartTime(startTime);
            this.currentTimeBlock.setEndTime(endTime);
        }

        showDetails();
        repaintView();
    }

    public void setEndTime(LocalTime endTime) {
        if (this.currentTimeBlock == null)
            throw new IllegalStateException("CurrentBusyTime is null!");

        if (currentTimeBlock.getStartTime().isBefore(endTime)) {
            this.currentTimeBlock.setEndTime(endTime);
        }

        showDetails();
        repaintView();
    }

    public void setDay(DayOfWeek day) {
        if (this.currentTimeBlock == null)
            throw new IllegalStateException("CurrentBusyTime is null!");

        this.currentTimeBlock.setDay(day);
        showDetails();
        repaintView();
    }

    ///////////////
    // Flags
    ///////////////

    public boolean isShowingBusyTimes() {
        return showingBusyTimes;
    }

    public void setShowingBusyTimes(boolean showingBusyTimes) {
        this.showingBusyTimes = showingBusyTimes;
        repaintView();
    }

    public boolean isShowingClassMeetings() {
        return showingClassMeetings;
    }

    public void setShowingClassMeetings(boolean showingClassMeetings) {
        this.showingClassMeetings = showingClassMeetings;
        repaintView();
    }

    public boolean isShowingSessions() {
        return showingSessions;
    }

    public void setShowingSessions(boolean showingSessions) {
        this.showingSessions = showingSessions;
        repaintView();
    }

    //////////////////
    // Control View
    //////////////////

    private void repaintView() {
        if (schedulePanel == null || instructorsPane == null)
            throw new AssertionError("setSchedulePanel and setInstructorsPanel must be called before repaintView");

        schedulePanel.onScheduleChanged();
        instructorsPane.onScheduleChanged();
        //schedulePanel.repaint();
    }

    public boolean isActiveSI(SupplementalInstructor si) {
        return activeSIs.contains(si);
    }

    public Collection<SupplementalInstructor> getActiveSIs() {
        return activeSIs;
    }

    ///////////////////////////
    // Observer functions
    ///////////////////////////

    public void observeScheduleComponents(ScheduleComponent... scheduleComponents) {
        for (ScheduleComponent component : scheduleComponents) {
            component.setController(this);
            observedScheduleComponents.add(component);
        }
    }
    
    public void notifyScheduleChange(){
        observedScheduleComponents.forEach(ScheduleComponent::onScheduleChanged);
        showDetails();
    }

    ///////////////////////////
    // Other getters/setters
    ///////////////////////////

    public SISchedule getSchedule() {
        return schedule;
    }

    public void setSchedule(SISchedule schedule) {
        logger.info("Changing Schedule: {}", schedule.getSupplementalInstructors());

        this.schedule = schedule;
        
        this.getSchedule().getSupplementalInstructors()
                .forEach(si -> instructorsPane.makeSIPanel(si));

        notifyScheduleChange();
    }
    
    public void addToSchedule(SISchedule schedule){
        List<SupplementalInstructor> tmp = new ArrayList<>(schedule.getSupplementalInstructors());
        for(SupplementalInstructor si : tmp){
            if(this.getSchedule().getSupplementalInstructors().contains(si)){
                schedule.getSupplementalInstructors().remove(si);
                schedule.getSessions().removeAll(schedule.getSessionsForSI(si));
            }
        }
        this.getSchedule().addSIs(schedule.getSupplementalInstructors());
        this.getSchedule().addSessions(schedule.getSessions());
        
        schedule.getSupplementalInstructors()
                .forEach(si -> instructorsPane.makeSIPanel(si));
        
        notifyScheduleChange();
    }
    
    public Session getCurrentSession(){
        return this.currentSession;
    }
    
    public TimeBlock getCurrentTimeBlock(){
        return this.currentTimeBlock;
    }
    
    public void lockSelectedSISessions(){
        for(SupplementalInstructor si : activeSIs){
            si.setLocked(true);
            for(Session session : schedule.getSessionsForSI(si)){
                session.setLockedSession(true);
            }
        }
        repaintView();
    }
    
    public void removeSI(SupplementalInstructor si){
        schedule.getSupplementalInstructors().remove(si);
        schedule.getSessions().remove(schedule.getSessionsForSI(si));
    }
}
