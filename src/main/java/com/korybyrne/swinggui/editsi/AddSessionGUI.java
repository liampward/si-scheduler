package com.korybyrne.swinggui.editsi;

import com.korybyrne.domain.Session;
import com.korybyrne.domain.si.SupplementalInstructor;
import com.korybyrne.swinggui.ScheduleMakerController;
import com.korybyrne.swinggui.details.DetailsPane;
import java.awt.BorderLayout;
import java.awt.CardLayout;
import java.awt.Dimension;
import java.awt.GridBagLayout;
import java.time.LocalTime;
import javax.swing.JFrame;
import javax.swing.JOptionPane;

public class AddSessionGUI {
    
    JFrame window;
    ScheduleMakerController controller;
    NewSessionPanel newSessionPanel;
    Session newSession;
    SupplementalInstructor si;
    
    public AddSessionGUI(ScheduleMakerController controller, SupplementalInstructor si){
        this.si = si;
        
        if(controller.getActiveSIs().isEmpty()){
            JOptionPane.showMessageDialog(null, "Please select an SI");
            return;
        } 
        this.controller = controller;
        
        newSession = new Session();
        newSession.setStartTime(LocalTime.of(9, 0));
        newSession.setEndTime(LocalTime.of(21, 0));
        newSession.setSupplementalInstructor(si);
        
        window = new JFrame("Add New Session");
        window.setLayout(new BorderLayout());
        //window.setResizable(true);
        window.setPreferredSize(new Dimension(200, 250));
        window.pack();
        window.setVisible(true);
        
        newSessionPanel = new NewSessionPanel(true, this);
        newSessionPanel.setController(controller);
        
        window.add(newSessionPanel);
    }
    
    public ScheduleMakerController getController(){
        return this.controller;
    }
    
    public Session getSession(){
        return this.newSession;
    }
    
    public SupplementalInstructor getSupplementalInstructor(){
        return this.si;
    }
}
