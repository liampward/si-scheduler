package com.korybyrne.swinggui.editsi;

import com.korybyrne.domain.course.Course;
import com.korybyrne.domain.si.SupplementalInstructor;
import com.korybyrne.swinggui.ScheduleMakerController;
import java.awt.GridLayout;
import java.util.Collections;
import java.util.List;
import javax.swing.DefaultListModel;
import javax.swing.JFrame;
import javax.swing.JList;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTabbedPane;
import javax.swing.ListSelectionModel;
import javax.swing.event.ListSelectionEvent;
import javax.swing.event.ListSelectionListener;

public class RequestCoursesPanel extends JPanel implements ListSelectionListener{
    
    private ScheduleMakerController controller;
    private SupplementalInstructor si;
    
    private JScrollPane courseListPane;
    private JScrollPane requestedListPane;
    
    private JList<Course> courseJList;
    private JList<Course> requestedCourses;
    private DefaultListModel<Course> requestModel;
    
    public RequestCoursesPanel(ScheduleMakerController controller, SupplementalInstructor si){
        this.controller = controller;
        this.si = si;
        
        this.setLayout(new GridLayout(0, 2));
        
        populateCourseList();
        populateRequestedCourseList();
    }
    
    void populateCourseList(){
        this.courseJList = new JList<>(controller.getSchedule().getCourseList().toArray(new Course[0]));
        this.courseJList.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);
        this.courseJList.addListSelectionListener(this);
        this.courseListPane = new JScrollPane(courseJList);
        this.add(courseListPane);
    }
    
    void populateRequestedCourseList(){
        requestModel = new DefaultListModel();
        for(Course course : si.getCourseSchedule().getCourses()){
            requestModel.addElement(course);
        }
        this.requestedCourses = new JList<>(requestModel);
        this.requestedCourses.addListSelectionListener(this);
        this.requestedCourses.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);
        this.requestedListPane = new JScrollPane(requestedCourses);

        this.add(requestedListPane);
    }
    
    public List<Course> getNewRequestedCourses(){
        return Collections.list(requestModel.elements());
    }

    @Override
    public void valueChanged(ListSelectionEvent e) {
        Object oSource = e.getSource();

        if(oSource == courseJList){
            JList<Course> source = (JList<Course>) oSource;
            
            Course selected = source.getSelectedValue();
            if(!requestModel.contains(selected))
                requestModel.addElement(selected);
        } else if(oSource == requestedCourses){
            JList<Course> source = (JList<Course>) oSource;
            
            Course selected = source.getSelectedValue();
            requestModel.removeElement(selected);
        }
    }
}
