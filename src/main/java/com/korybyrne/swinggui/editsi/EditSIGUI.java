package com.korybyrne.swinggui.editsi;

import com.korybyrne.domain.Session;
import com.korybyrne.domain.course.Course;
import com.korybyrne.domain.si.RequestedHours;
import com.korybyrne.domain.si.SupplementalInstructor;
import com.korybyrne.swinggui.ScheduleMakerController;
import java.awt.BorderLayout;
import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.text.FieldPosition;
import java.text.NumberFormat;
import java.text.ParseException;
import java.text.ParsePosition;
import java.time.Duration;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javafx.scene.control.SpinnerValueFactory;
import javax.swing.BoxLayout;
import javax.swing.JButton;
import javax.swing.JFormattedTextField;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JSpinner;
import javax.swing.JTextField;
import javax.swing.SpinnerNumberModel;

public class EditSIGUI {
    
    private JFrame window;
    private ScheduleMakerController controller;
    private SupplementalInstructor si;
    
    private JPanel mainPanel;
    private JPanel infoPanel;
    private RequestCoursesPanel requestedCoursesPanel;

    private JTextField newName;
    private JFormattedTextField newID;
    private JSpinner minHours;
    private JSpinner maxHours;
    private JButton addSession;
    private JButton save;
    private JButton delete;
    
    private NumberFormat idFormat;
    
    public EditSIGUI(ScheduleMakerController controller, SupplementalInstructor si){
        this.controller = controller;
        
        this.si = si;
        
        mainPanel = new JPanel();
        mainPanel.setLayout(new FlowLayout());
        
        infoPanel = new JPanel();
        infoPanel.setLayout(new GridLayout(0, 2));
        
        requestedCoursesPanel = new RequestCoursesPanel(controller, si);
        
        infoPanel.add(new JLabel("Name: "));
        newName = new JTextField(si.getName());
        newName.setPreferredSize(new Dimension(150, 24));
        infoPanel.add(newName);
        
        infoPanel.add(new JLabel("ID: "));;
        idFormat = NumberFormat.getNumberInstance();
        idFormat.setMaximumIntegerDigits(6);
        newID = new JFormattedTextField(idFormat);
        newID.setValue(si.getSsuId());
        infoPanel.add(newID);
        
        SpinnerNumberModel spinnerModelMin = new SpinnerNumberModel(1, 1, 20, 1);
        SpinnerNumberModel spinnerModelMax = new SpinnerNumberModel(1, 1, 20, 1);
        
        infoPanel.add(new JLabel("Min Hours: "));
        minHours = new JSpinner(spinnerModelMin);
        minHours.setValue((int)si.getMinimumRequestedHours());
        infoPanel.add(minHours);
        
        infoPanel.add(new JLabel("Max Hours:"));
        maxHours = new JSpinner(spinnerModelMax);
        maxHours.setValue((int)si.getMaximumRequestedHours());
        infoPanel.add(maxHours);
        
        addSession = new JButton("Add Session");
        addSession.addActionListener(new ActionListener(){
            @Override
            public void actionPerformed(ActionEvent e) {
                new AddSessionGUI(controller, si);
            }
        });
        infoPanel.add(addSession);
        
        delete = new JButton("Delete SI");
        delete.addActionListener(new ActionListener(){
            @Override
            public void actionPerformed(ActionEvent e) {
                String[] options = {"Yes", "No"};
                int accept = JOptionPane.showOptionDialog(infoPanel,
                        "Are you sure you want to delete " + si.getName(),
                        "Delete SI?",
                        JOptionPane.YES_NO_OPTION,
                        JOptionPane.QUESTION_MESSAGE,
                        null,
                        options,
                        options[0]);
                
                if(accept == 0){
                    controller.removeSI(si);
                    controller.getActiveSIs().remove(si);
                    controller.notifyScheduleChange();

                    window.dispose();
                }
            }
        });
        infoPanel.add(delete);
        
        save = new JButton("Save");
        save.addActionListener(new ActionListener(){
            @Override
            public void actionPerformed(ActionEvent e) {
                if((int)minHours.getValue() > (int)maxHours.getValue()){
                    JOptionPane.showMessageDialog(null, "Minimum hours are greater than maximum!");
                    return;
                } else if(newID.getText().length() < 6){
                    JOptionPane.showMessageDialog(null, "SSU IDs are six digits in length.");
                    return;
                }
                
                si.setName(newName.getText());
                
                try {
                    si.setSsuId(idFormat.parse(newID.getText()).intValue());
                } catch (ParseException ex) {
                    Logger.getLogger(EditSIGUI.class.getName()).log(Level.SEVERE, null, ex);
                }
                
                RequestedHours newHours = new RequestedHours(
                        Duration.ofHours((int) minHours.getValue()),
                        Duration.ofHours((int) maxHours.getValue()));
                
                si.setRequestedHours(newHours);
                
                si.setRequestedClasses(requestedCoursesPanel.getNewRequestedCourses());
                controller.getSchedule().updateSessionsForSI(si);
                
                controller.notifyScheduleChange();
                window.dispose();
            }
        });
        infoPanel.add(save);
        
        mainPanel.add(infoPanel);
        mainPanel.add(requestedCoursesPanel);
        
        window = new JFrame("Edit SI: " + si.getName());
        window.setLayout(new BorderLayout());
        //window.setResizable(true);
        window.setPreferredSize(new Dimension(520, 210));
        window.pack();
        window.setVisible(true);
        
        window.add(mainPanel);
    }
    
    public ScheduleMakerController getController(){
        return this.controller;
    }
    
}
