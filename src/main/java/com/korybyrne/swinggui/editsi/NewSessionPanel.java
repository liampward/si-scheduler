package com.korybyrne.swinggui.editsi;

import com.korybyrne.domain.Session;
import com.korybyrne.domain.course.Course;
import com.korybyrne.domain.si.SupplementalInstructor;
import com.korybyrne.swinggui.ScheduleMakerController;
import com.korybyrne.swinggui.details.TimeBlockDetailsPanel;
import java.awt.FlowLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.ItemEvent;
import java.awt.event.ItemListener;
import java.time.DayOfWeek;
import java.time.Duration;
import java.time.LocalTime;
import java.util.Collection;
import java.util.Collections;
import java.util.stream.Collectors;
import javax.swing.BoxLayout;
import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;


public class NewSessionPanel extends TimeBlockDetailsPanel{
    
    AddSessionGUI parent;
    
    private JPanel coursePanel;
    private JLabel courseLabel;
    protected JComboBox<Course> courseSelector;
    
    private JButton submit;
    
    private boolean ignoreChange;
    
    public NewSessionPanel(){
        super();
    }
    
    public NewSessionPanel(boolean isEnabled, AddSessionGUI parent){
        super(isEnabled);
        
        this.parent = parent;
        this.controller = parent.getController();
        
        coursePanel = new JPanel(new FlowLayout(FlowLayout.LEADING));
        submit = new JButton();
        submit.setVisible(false);
        
        submit = new JButton("Add Session");
        submit.setVisible(true);
        submit.addActionListener(new ActionListener(){
            @Override
            public void actionPerformed(ActionEvent e) {
                controller.getSchedule().addSessions(Collections.singletonList(parent.getSession()));
                parent.window.dispose();
                controller.notifyScheduleChange();
            }
        });

        courseSelector = makeCourseComboBox();

        courseLabel = new JLabel("Course:");
        coursePanel.add(courseLabel);
        coursePanel.add(courseSelector);

        this.courseSelector.setSelectedIndex(0);

        this.startTimeSelector.setSelectedItem(parent.getSession().getStartTime());
        this.endTimeSelector.setSelectedItem(parent.getSession().getEndTime());
        this.daySelector.setSelectedItem(parent.getSession().getDay());
        
        this.add(coursePanel);
        this.add(submit);
    }

    private JComboBox<Course> makeCourseComboBox(){
        JComboBox<Course> comboBox = new JComboBox<>();
        
        parent.getSupplementalInstructor().getCourseSchedule().getCourses()
                .forEach((course) -> {comboBox.addItem(course); });
        
        configureComboBox(comboBox);
        return comboBox;
    }
    
    @Override
    public void itemStateChanged(ItemEvent e) {
        Object source = e.getSource();
        
        if(ignoreChanges){
           return;
        }
        
        LocalTime startTime = (LocalTime) startTimeSelector.getSelectedItem();
        LocalTime endTime = (LocalTime) endTimeSelector.getSelectedItem();
        
        
        if(source == startTimeSelector){
            if(endTime != null && endTime.compareTo(startTime) < 0){
                startTime = endTime.minusMinutes(30);
                ignoreChanges = true;
                startTimeSelector.setSelectedItem(startTime);
            }
            ignoreChanges = false;
            parent.getSession().setStartTime(startTime);
            
            Duration newDuration = (endTime != null)? Duration.between(startTime, endTime)
                    : Duration.ofHours(12);
            parent.getSession().setDuration(newDuration);
        } else if(source == endTimeSelector){
            if(endTime.compareTo(startTime) < 0){
                endTime = startTime.plusMinutes(30);
                ignoreChanges = true;
                endTimeSelector.setSelectedItem(endTime);
            }
            ignoreChanges = false;
            parent.getSession().setEndTime(endTime);
        } else if(source == daySelector){
            parent.getSession().setDay((DayOfWeek) daySelector.getSelectedItem());
        } else if(source == courseSelector){
            parent.getSession().getCourses().clear();
            parent.getSession().getCourses().add((Course) courseSelector.getSelectedItem());
        }

    }
}