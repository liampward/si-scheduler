
package com.korybyrne.swinggui.renderer;

import java.awt.Component;
import java.time.LocalTime;
import java.time.format.DateTimeFormatter;
import java.time.format.FormatStyle;
import javax.swing.JList;
import javax.swing.plaf.basic.BasicComboBoxRenderer;

public class TimeSelectorRenderer extends BasicComboBoxRenderer{
    @Override
    public Component getListCellRendererComponent(JList list, Object value, int index,
            boolean isSelected, boolean cellHasFocus){
        
        super.getListCellRendererComponent(list, value, index, isSelected, cellHasFocus);
        
        LocalTime time = (LocalTime) value;
        
        if(time != null)
            setText(time.format(DateTimeFormatter.ofLocalizedTime(FormatStyle.SHORT)));
        
        return this;
    }
}
