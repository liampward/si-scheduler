package com.korybyrne.swinggui.details;

import com.korybyrne.swinggui.button.TimeBlockDeleteButton;
import com.korybyrne.domain.Session;
import com.korybyrne.swinggui.ScheduleComponent;
import com.korybyrne.swinggui.ScheduleMakerController;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

class SessionDetailsPanel extends JPanel implements ScheduleComponent {
    private JLabel titleLabel;
    private TimeBlockDetailsPanel timeBlockPanel;
    private TimeBlockDeleteButton deleteSessionButton;

    SessionDetailsPanel() {
        this.setLayout(new BoxLayout(this, BoxLayout.PAGE_AXIS));

        this.titleLabel = new JLabel();
        this.timeBlockPanel = new TimeBlockDetailsPanel();
        this.deleteSessionButton = new TimeBlockDeleteButton("Delete Session");

        this.add(titleLabel);
        this.add(timeBlockPanel);
        this.add(deleteSessionButton);
        this.add(new Box.Filler(new Dimension(0, 0), new Dimension(0, Integer.MAX_VALUE), new Dimension(0, Integer.MAX_VALUE)));
    }

    void prepareToShow(Session session) {
        this.titleLabel.setText(session.getSupplementalInstructor().getName());
        this.timeBlockPanel.displayCurrentTimeBlock(session);
    }

    @Override
    public void setController(ScheduleMakerController controller) {
        this.timeBlockPanel.setController(controller);
        this.deleteSessionButton.setController(controller);
    }
}
