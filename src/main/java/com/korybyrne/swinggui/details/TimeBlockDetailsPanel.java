package com.korybyrne.swinggui.details;

import com.korybyrne.domain.TimeBlock;
import com.korybyrne.swinggui.renderer.TimeSelectorRenderer;
import com.korybyrne.swinggui.ScheduleComponent;
import com.korybyrne.swinggui.ScheduleMakerController;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ItemEvent;
import java.awt.event.ItemListener;
import java.time.DayOfWeek;
import java.time.LocalTime;
import java.util.stream.Collectors;
import java.util.stream.IntStream;

public class TimeBlockDetailsPanel extends JPanel implements ItemListener, ScheduleComponent {
    private Logger logger = LoggerFactory.getLogger(getClass());
    protected ScheduleMakerController controller;
    
    private JPanel startTimePanel;
    private JLabel startTimeLabel;
    protected JComboBox<LocalTime> startTimeSelector;
    
    private JPanel endTimePanel;
    private JLabel endTimeLabel;
    protected JComboBox<LocalTime> endTimeSelector;
    
    private JPanel dayPanel;
    private JLabel dayLabel;
    protected JComboBox<DayOfWeek> daySelector;
    
    static TimeSelectorRenderer timeSelectorRenderer;

    public boolean ignoreChanges = false;
    private boolean isEnabled;

    public TimeBlockDetailsPanel()
    {
        this(true);
    }
    
    public TimeBlockDetailsPanel(boolean isEnabled)
    {
        
        this.setLayout(new BoxLayout(this, BoxLayout.PAGE_AXIS));
        this.isEnabled = isEnabled;
        
        startTimePanel = new JPanel(new FlowLayout(FlowLayout.LEADING));
        {
            startTimeLabel = new JLabel("Start Time:");

            startTimeSelector = makeTimeComboBox();

            startTimePanel.add(startTimeLabel);
            startTimePanel.add(startTimeSelector);
        }

        endTimePanel = new JPanel(new FlowLayout(FlowLayout.LEADING));
        {
            endTimeLabel = new JLabel("End Time:");
            endTimeSelector = makeTimeComboBox();

            endTimePanel.add(endTimeLabel);
            endTimePanel.add(endTimeSelector);
        }

        dayPanel = new JPanel(new FlowLayout(FlowLayout.LEADING));
        {
            dayLabel = new JLabel("Day:");

            daySelector = new JComboBox<>(DayOfWeek.values());
            configureComboBox(daySelector);

            dayPanel.add(dayLabel);
            dayPanel.add(daySelector);
        }

        this.add(startTimePanel);
        this.add(endTimePanel);
        this.add(dayPanel);
    }

    @Override
    public void itemStateChanged(ItemEvent itemEvent)
    {
        if (ignoreChanges) { return; }

        logger.info("Value Changed: " + itemEvent);

        Object source = itemEvent.getSource();

        if (source == startTimeSelector) {
            controller.setStartTime((LocalTime) startTimeSelector.getSelectedItem());
        } else if (source == endTimeSelector) {
            controller.setEndTime((LocalTime) endTimeSelector.getSelectedItem());
        } else if (source == daySelector) {
            controller.setDay((DayOfWeek) daySelector.getSelectedItem());
        }
    }

    void displayCurrentTimeBlock(TimeBlock timeBlock)
    {
        if (timeBlock == null) {
            throw new IllegalStateException("displayCurrentTimeBlock should never be called with currentBusyTime null");
        }

        ignoreChanges = true;
        {
            addIfAbsent(startTimeSelector, timeBlock.getStartTime());
            addIfAbsent(endTimeSelector, timeBlock.getEndTime());
            
            startTimeSelector.setSelectedItem(timeBlock.getStartTime());
            endTimeSelector.setSelectedItem(timeBlock.getEndTime());
            daySelector.setSelectedItem(timeBlock.getDay());
        }
        ignoreChanges = false;

        this.revalidate();
        this.repaint();
    }

    protected void configureComboBox(JComboBox comboBox) {
        comboBox.setEnabled(isEnabled);
        comboBox.setSelectedIndex(-1);
        comboBox.addItemListener(this);
    }

    private JComboBox<LocalTime> makeTimeComboBox() {
        JComboBox<LocalTime> comboBox = new JComboBox<>(IntStream.range(18, 43)
                .mapToObj(halfHour -> LocalTime.of(halfHour/2, 30 * (halfHour % 2)))
                .collect(Collectors.toList()).toArray(new LocalTime[24])
        );
        
        configureComboBox(comboBox);
        
        if(timeSelectorRenderer == null)
            timeSelectorRenderer = new TimeSelectorRenderer();
        
        comboBox.setRenderer(timeSelectorRenderer);
        
        return comboBox;
    }

    private static <E> void addIfAbsent(JComboBox<E> comboBox, E item) {
        if (((DefaultComboBoxModel) comboBox.getModel()).getIndexOf(item) == -1) {
            comboBox.addItem(item);
        }
    }

    @Override
    public void setController(ScheduleMakerController controller) {
        this.controller = controller;
    }
}
