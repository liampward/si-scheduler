package com.korybyrne.swinggui.details;

import com.korybyrne.swinggui.button.TimeBlockDeleteButton;
import com.korybyrne.domain.TimeBlock;
import com.korybyrne.swinggui.ScheduleComponent;
import com.korybyrne.swinggui.ScheduleMakerController;

import javax.swing.*;
import java.awt.*;

class BusyDetailsPanel extends JPanel implements ScheduleComponent {
    private TimeBlockDetailsPanel timeBlockPanel;
    private TimeBlockDeleteButton deleteBusyButton;

    BusyDetailsPanel()
    {
        this.setLayout(new BoxLayout(this, BoxLayout.PAGE_AXIS));

        timeBlockPanel = new TimeBlockDetailsPanel();
        deleteBusyButton = new TimeBlockDeleteButton("Delete");

        this.add(timeBlockPanel);
        this.add(deleteBusyButton);
        this.add(new Box.Filler(new Dimension(0, 0), new Dimension(0, Integer.MAX_VALUE), new Dimension(0, Integer.MAX_VALUE)));
    }

    void prepareToShow(TimeBlock timeBlock)
    {
        timeBlockPanel.displayCurrentTimeBlock(timeBlock);
    }

    @Override
    public void setController(ScheduleMakerController controller) {
        this.timeBlockPanel.setController(controller);
        this.deleteBusyButton.setController(controller);
    }
}
