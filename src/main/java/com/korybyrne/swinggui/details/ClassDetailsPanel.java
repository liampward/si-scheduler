package com.korybyrne.swinggui.details;

import com.korybyrne.domain.course.ClassMeeting;
import com.korybyrne.domain.course.Course;
import com.korybyrne.domain.course.Section;
import com.korybyrne.swinggui.ScheduleComponent;
import com.korybyrne.swinggui.ScheduleMakerController;
import com.korybyrne.swinggui.ScheduleMakerGUI;

import javax.swing.*;
import java.awt.*;

import static com.korybyrne.swinggui.ScheduleMakerGUI.decimalFormatter;

class ClassDetailsPanel extends JPanel implements ScheduleComponent {
    private ScheduleMakerController controller;

    private JLabel titleLabel;
    private TimeBlockDetailsPanel timeBlockPanel;
    private JPanel siHoursPanel;
    private JLabel siHoursLabel;

    ClassDetailsPanel() {
        this.setLayout(new BoxLayout(this, BoxLayout.PAGE_AXIS));

        titleLabel = new JLabel();
        timeBlockPanel = new TimeBlockDetailsPanel(false);
        siHoursPanel = new JPanel(new FlowLayout(FlowLayout.LEADING));
        siHoursLabel = new JLabel();

        siHoursPanel.add(new JLabel("SI Hours:"));
        siHoursPanel.add(siHoursLabel);

        this.add(titleLabel);
        this.add(timeBlockPanel);
        this.add(siHoursPanel);
        this.add(new Box.Filler(new Dimension(0, 0), new Dimension(0, Integer.MAX_VALUE), new Dimension(0, Integer.MAX_VALUE)));
    }

    void prepareToShow(Course course, Section section, ClassMeeting meeting) {
        titleLabel.setText(meeting.toString());
        timeBlockPanel.displayCurrentTimeBlock(meeting);
        siHoursLabel.setText(
                decimalFormatter.format(controller.getSchedule().getSIHoursForCourse(course))
        );
    }

    @Override
    public void setController(ScheduleMakerController controller) {
        this.controller = controller;
        timeBlockPanel.setController(controller);
    }
}
