package com.korybyrne.swinggui.instructors;

import com.korybyrne.domain.si.SupplementalInstructor;
import com.korybyrne.swinggui.ScheduleMakerController;
import com.korybyrne.swinggui.ScheduleComponent;

import javax.swing.*;
import java.awt.*;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class InstructorsPane extends JScrollPane implements ScheduleComponent {
    private final Logger logger = LoggerFactory.getLogger(getClass());
    
    private ScheduleMakerController controller;
    private JPanel instructorContainer;

    private SupplementalInstructor si;

    public InstructorsPane() {
        super(ScrollPaneConstants.VERTICAL_SCROLLBAR_NEVER, ScrollPaneConstants.HORIZONTAL_SCROLLBAR_AS_NEEDED);
        this.instructorContainer = new JPanel();

        this.setViewportView(instructorContainer);

        instructorContainer.setBackground(Color.ORANGE);
    }

    @Override
    public void onScheduleChanged() {
        
        for(Component comp : instructorContainer.getComponents()){
            InstructorPanel compAsInstructorPanel = (InstructorPanel)comp;
            
            if(this.controller.getActiveSIs().contains(compAsInstructorPanel.getSI())){
                compAsInstructorPanel.setVisible(true);
            } else{
                compAsInstructorPanel.setVisible(false);
            }
            
            compAsInstructorPanel.onScheduleChanged();
        }

        this.revalidate();
        this.repaint();
    }

    public void makeSIPanel(SupplementalInstructor si) {
        if(instructorPanelExists(si))
            return;
        
        InstructorPanel siPanel = new InstructorPanel(si);
        siPanel.setController(controller);
        siPanel.setupPanel();
        instructorContainer.add(siPanel);
    }

    @Override
    public void setController(ScheduleMakerController controller) {
        this.controller = controller;
    }

    private boolean instructorPanelExists(SupplementalInstructor si){
        for(Component comp : instructorContainer.getComponents()){
            InstructorPanel compAsInstructorPanel = (InstructorPanel)comp;
            
            if(compAsInstructorPanel.getSI().equals(si)){
                return true;
            }
        }
        return false;
    }
}
