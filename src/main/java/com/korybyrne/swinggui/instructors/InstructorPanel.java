package com.korybyrne.swinggui.instructors;

import com.korybyrne.domain.Session;
import com.korybyrne.domain.si.SupplementalInstructor;
import com.korybyrne.swinggui.ScheduleComponent;
import com.korybyrne.swinggui.ScheduleMakerController;
import static com.korybyrne.swinggui.ScheduleMakerGUI.decimalFormatter;
import com.korybyrne.swinggui.button.EditSIButton;
import java.awt.Color;
import java.awt.Component;
import java.awt.event.ItemEvent;
import java.awt.event.ItemListener;
import java.util.Collection;
import javax.swing.BorderFactory;
import javax.swing.BoxLayout;
import javax.swing.JButton;
import javax.swing.JCheckBox;
import javax.swing.JLabel;
import javax.swing.JPanel;


public class InstructorPanel extends JPanel implements ItemListener, ScheduleComponent{
    private ScheduleMakerController controller;
    private SupplementalInstructor si;
    private JCheckBox checkBox;
    private EditSIButton editSIButton;
    
    public InstructorPanel(SupplementalInstructor si){
        this.si = si;
    }
    
    public SupplementalInstructor getSI(){
        return this.si;
    }
    
    public void setupPanel(){
        if(controller == null){
            //throw illegal state exception
            System.out.println("WhoOPs");
            return;
        }
        Collection<Session> siSessions = controller.getSchedule().getSessionsForSI(si);
        double hours = siSessions.stream().mapToDouble(Session::getHours).sum();

        this.setLayout(new BoxLayout(this, BoxLayout.PAGE_AXIS));
        this.setBorder(BorderFactory.createLineBorder(Color.BLACK));

        this.add(new JLabel(si.getName() + " : " + si.getSsuId()));
        this.add(new JLabel("Total SI Hours: " + decimalFormatter.format(hours)));
        this.add(new JLabel("Total Sessions: " + siSessions.size()));
        this.add(new JLabel("Minimum Requested Hours: " + decimalFormatter.format(si.getMinimumRequestedHours())));
        this.add(new JLabel("Minimum Requested Hours: " + decimalFormatter.format(si.getMaximumRequestedHours())));
        
        checkBox = new JCheckBox("Lock sechedule");
        checkBox.addItemListener(this);
        this.add(checkBox);
        
        editSIButton = new EditSIButton("Edit SI", this.si);
        editSIButton.setController(controller);
        
        this.add(editSIButton);
    }
    
    @Override
    public void itemStateChanged(ItemEvent e) {
        
        Object source = e.getSource();
        
        if(source == checkBox){
            if(e.getStateChange() == 1){ //selected
                si.setLocked(true);
                controller.getSchedule().getSessionsForSI(si)
                        .forEach(session -> session.setLockedSession(true));
                System.out.println(controller.getSchedule().getSessionsForSI(si).size());
                System.out.println(controller.getSchedule().getSessions().size());
            } else{ //unselected
                si.setLocked(false);
                controller.getSchedule().getSessionsForSI(si)
                        .forEach(session -> session.setLockedSession(false));
                System.out.println(controller.getSchedule().getSessionsForSI(si).size());
            }
        } else if(source == editSIButton){
        }
    }

    @Override
    public void setController(ScheduleMakerController controller) {
        this.controller = controller;
    }
    
    @Override
    public void onScheduleChanged(){
        Collection<Session> siSessions = controller.getSchedule().getSessionsForSI(si);
        double hours = siSessions.stream().mapToDouble(Session::getHours).sum();
        
        Component[] components = this.getComponents();
        ((JLabel)getComponent(0)).setText(si.getName() + " : " + si.getSsuId());
        ((JLabel)getComponent(1)).setText("Total SI Hours: " + decimalFormatter.format(hours));
        ((JLabel)getComponent(2)).setText("Total Sessions: " + siSessions.size());
        ((JLabel)getComponent(3)).setText("Minimum Requested Hours: " + decimalFormatter.format(si.getMinimumRequestedHours()));
        ((JLabel)getComponent(4)).setText("Minimum Requested Hours: " + decimalFormatter.format(si.getMaximumRequestedHours()));
        checkBox.setSelected(si.getLocked());
    }
}
