package com.korybyrne.swinggui.configuration;

import com.korybyrne.domain.si.SupplementalInstructor;
import com.korybyrne.swinggui.ScheduleMakerController;
import com.korybyrne.swinggui.ScheduleComponent;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.swing.*;
import java.awt.*;

public class ConfigurationPanel extends JPanel implements ScheduleComponent {
    private Logger logger = LoggerFactory.getLogger(getClass());

    private CheckboxPanel checkboxPanel;

    private ScheduleMakerController controller;

    public ConfigurationPanel() {
        this.checkboxPanel = new CheckboxPanel();

        this.setLayout(new FlowLayout());
        this.add(checkboxPanel);
    }

    @Override
    public void setController(ScheduleMakerController controller) {
        this.controller = controller;
        this.checkboxPanel.setController(this.controller);
    }
}
