package com.korybyrne.swinggui.button;

import com.korybyrne.domain.si.SupplementalInstructor;
import com.korybyrne.swinggui.ScheduleComponent;
import com.korybyrne.swinggui.ScheduleMakerController;
import com.korybyrne.swinggui.editsi.EditSIGUI;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javax.swing.JButton;

public class EditSIButton extends JButton implements ScheduleComponent{
    
    ScheduleMakerController controller;
    SupplementalInstructor si;
    
    public EditSIButton(String text, SupplementalInstructor si){
        super(text);
        this.si = si;
        this.addActionListener(new ActionListener(){
            @Override
            public void actionPerformed(ActionEvent e) {
                /*
                controller.removeSI(si);
                controller.getActiveSIs().remove(si);
                controller.notifyScheduleChange();
                */
                new EditSIGUI(controller, si);
            }
        });
    }

    @Override
    public void setController(ScheduleMakerController controller) {
        this.controller = controller;
    }
}
