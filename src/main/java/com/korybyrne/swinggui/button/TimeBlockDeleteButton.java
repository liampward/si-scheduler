/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.korybyrne.swinggui.button;

import com.korybyrne.domain.Session;
import com.korybyrne.domain.TimeBlock;
import com.korybyrne.swinggui.ScheduleComponent;
import com.korybyrne.swinggui.ScheduleMakerController;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.time.DayOfWeek;
import java.time.Duration;
import javax.swing.JButton;

/**
 *
 * @author Liam Ward
 */
public class TimeBlockDeleteButton extends JButton implements ScheduleComponent{

    private ScheduleMakerController controller;

    public TimeBlockDeleteButton(String text) {
        super(text);
        this.addActionListener(new ActionListener(){
            @Override
            public void actionPerformed(ActionEvent e) {
                TimeBlock timeBlock = controller.getCurrentTimeBlock();
                
                timeBlock.setDuration(Duration.ZERO);
                
                if(controller.getCurrentTimeBlock().getClass() == Session.class){
                    controller.getSchedule().getSessions().remove((Session) timeBlock);
                } else{
                    
                }
                
                controller.onNoneClicked();
                controller.notifyScheduleChange();
            }
            
        });
    }

    @Override
    public void setController(ScheduleMakerController controller) {
        this.controller = controller;
    }
}
