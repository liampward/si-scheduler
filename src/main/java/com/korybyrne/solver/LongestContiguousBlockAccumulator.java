package com.korybyrne.solver;

import com.korybyrne.domain.Session;
import org.kie.api.runtime.rule.AccumulateFunction;

import java.io.*;
import java.time.Duration;
import java.time.LocalTime;
import java.util.*;
import java.util.stream.Collectors;

public class LongestContiguousBlockAccumulator implements AccumulateFunction<LongestContiguousBlockAccumulator.Data> {

    public void readExternal(ObjectInput in) throws IOException, ClassNotFoundException {

    }

    public void writeExternal(ObjectOutput out) throws IOException {

    }

    public static class Data implements Serializable {
        public SortedSet<Session> sortedSessions; // sorted by start time, overlap is assumed to be handled in drools

        public void init() {
            sortedSessions = new TreeSet<>(Comparator.comparing(Session::getStartTime));
        }
    }

    /* (non-Javadoc)
     * @see org.kie.api.runtime.rule.AccumulateFunction#createContext()
     */
    public LongestContiguousBlockAccumulator.Data createContext() {
        return new LongestContiguousBlockAccumulator.Data();
    }

    /* (non-Javadoc)
     * @see org.kie.api.runtime.rule.AccumulateFunction#init(java.io.Serializable)
     */
    public void init(LongestContiguousBlockAccumulator.Data context) {
        context.init();
    }

    /* (non-Javadoc)
     * @see org.kie.api.runtime.rule.AccumulateFunction#accumulate(java.io.Serializable, java.lang.Object)
     */
    public void accumulate(LongestContiguousBlockAccumulator.Data context,
                           Object value) {
        if (! (value instanceof Session)) {
            throw new RuntimeException("Input to LongestContiguousBlockAccumulator must be of type <Session>!");
        }
        
        context.sortedSessions.add((Session) value);
    }

    /* (non-Javadoc)
     * @see org.kie.api.runtime.rule.AccumulateFunction#reverse(java.io.Serializable, java.lang.Object)
     */

    public void reverse(LongestContiguousBlockAccumulator.Data context, Object value) {
        if (! (value instanceof Session)) {
            throw new RuntimeException("Input to LongestContiguousBlockAccumulator must be of type <Session>!");
        }

        context.sortedSessions.remove(value);
    }

    /* (non-Javadoc)
     * @see org.kie.api.runtime.rule.AccumulateFunction#getResult(java.io.Serializable)
     */
    public Object getResult(LongestContiguousBlockAccumulator.Data context) {
        Duration longestDuration = Duration.ofSeconds(0);
        Duration currentDuration = Duration.ofSeconds(0);
        LocalTime previousEndTime = null;

        for (Session session : context.sortedSessions) {
            if (session.getStartTime().equals(previousEndTime)) {
                currentDuration = currentDuration.plus(session.getDuration());
            } else {
                currentDuration = session.getDuration();
            }

            previousEndTime = session.getEndTime();

            if (currentDuration.compareTo(longestDuration) >= 0) {
                longestDuration = currentDuration;
            }
        }

        return longestDuration.getSeconds() / 3600.0;
    }

    /* (non-Javadoc)
     * @see org.kie.api.runtime.rule.AccumulateFunction#supportsReverse()
     */
    public boolean supportsReverse() {
        return true;
    }

    /* (non-Javadoc)
     * @see org.kie.api.runtime.rule.AccumulateFunction#getResultType()
     */
    public Class< ? > getResultType() {
        return Double.class;
    }

}
