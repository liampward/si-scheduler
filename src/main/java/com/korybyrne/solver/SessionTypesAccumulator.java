package com.korybyrne.solver;

import com.korybyrne.domain.Session;
import com.korybyrne.domain.course.Course;
import org.kie.api.runtime.rule.AccumulateFunction;

import java.io.*;
import java.time.Duration;
import java.util.*;
import java.util.stream.Collectors;

public class SessionTypesAccumulator implements AccumulateFunction<SessionTypesAccumulator.Data> {

    public void readExternal(ObjectInput in) throws IOException, ClassNotFoundException {

    }

    public void writeExternal(ObjectOutput out) throws IOException {

    }

    public static class Data implements Externalizable {
        public Map<Set<String>, Duration> hoursPerSessionType;

        public void readExternal(ObjectInput in) throws IOException, ClassNotFoundException {
            int numEntries = in.readInt();
            Map<Set<String>, Duration> newHoursPerSessionType = new HashMap<>(numEntries);

            for (int i = 0; i < numEntries; ++i) {
                int numSetEntries = in.readInt();
                Set<String> newSessionType = new HashSet<>(numSetEntries);

                for (int j = 0; j < numSetEntries; ++j) {
                    newSessionType.add((String) in.readObject());
                }

                long seconds = in.readLong();
                newHoursPerSessionType.put(newSessionType, Duration.ofSeconds(seconds));
            }

            this.hoursPerSessionType = newHoursPerSessionType;
        }

        public void writeExternal(ObjectOutput out) throws IOException {
            out.writeInt(hoursPerSessionType.size());

            for (Map.Entry<Set<String>, Duration> entry : hoursPerSessionType.entrySet()) {
                out.writeInt(entry.getKey().size());

                for (String courseCode : entry.getKey()) {
                    out.writeObject(courseCode);
                }

                out.writeLong(entry.getValue().getSeconds());
            }
        }

        public void init() {
            hoursPerSessionType = new HashMap<>();
        }
    }

    /* (non-Javadoc)
     * @see org.kie.api.runtime.rule.AccumulateFunction#createContext()
     */
    public Data createContext() {
        return new Data();
    }

    /* (non-Javadoc)
     * @see org.kie.api.runtime.rule.AccumulateFunction#init(java.io.Serializable)
     */
    public void init(Data context) {
        context.init();
    }

    /* (non-Javadoc)
     * @see org.kie.api.runtime.rule.AccumulateFunction#accumulate(java.io.Serializable, java.lang.Object)
     */
    public void accumulate(Data context,
                           Object value) {
        if (! (value instanceof Session)) {
            throw new RuntimeException("Input to SessionTypesAccumulator must be of type <Session>!");
        }

        Session session = (Session) value;
        Set<String> sessionType = session.getSessionType();
        Duration previousDuration = context.hoursPerSessionType.getOrDefault(sessionType, Duration.ofSeconds(0));

        context.hoursPerSessionType.put(
                sessionType, previousDuration.plus(session.getDuration())
        );
    }

    /* (non-Javadoc)
     * @see org.kie.api.runtime.rule.AccumulateFunction#reverse(java.io.Serializable, java.lang.Object)
     */

    // TODO:kb figure out why reverse doesn't work
    public void reverse(Data context, Object value) {
        if (! (value instanceof Session)) {
            throw new RuntimeException("Input to SessionTypesAccumulator must be of type <Session>!");
        }

        Session session = (Session) value;
        Set<String> sessionType = session.getSessionType();
        Duration previousDuration = context.hoursPerSessionType.getOrDefault(sessionType, Duration.ofSeconds(0));

        Duration newDuration = previousDuration.minus(session.getDuration());

        if (newDuration.isZero()) {
            context.hoursPerSessionType.remove(sessionType);
        } else {
            context.hoursPerSessionType.put(sessionType, newDuration);
        }
    }

    /* (non-Javadoc)
     * @see org.kie.api.runtime.rule.AccumulateFunction#getResult(java.io.Serializable)
     */
    public Object getResult(Data context) {
        return context.hoursPerSessionType.values().stream()
                .map(Duration::getSeconds)
                .map(aLong -> aLong / 3600.0)
                .collect(Collectors.toList());
    }

    /* (non-Javadoc)
     * @see org.kie.api.runtime.rule.AccumulateFunction#supportsReverse()
     */
    public boolean supportsReverse() {
        return false;
    }

    /* (non-Javadoc)
     * @see org.kie.api.runtime.rule.AccumulateFunction#getResultType()
     */
    public Class< ? > getResultType() {
        return List.class;
    }

}
