package com.korybyrne.domain;

import org.apache.commons.lang3.builder.EqualsBuilder;
import org.apache.commons.lang3.builder.HashCodeBuilder;

import java.io.Serializable;
import java.time.DayOfWeek;
import java.time.Duration;
import java.time.LocalTime;
import java.time.format.DateTimeFormatter;
import java.time.format.FormatStyle;
import java.time.format.TextStyle;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.Locale;

public class TimeBlock implements Serializable {
    private DayOfWeek day;
    private LocalTime startTime;
    private Duration  duration;

    public TimeBlock() {
        this(DayOfWeek.MONDAY, LocalTime.of(9, 0), Duration.ofHours(0));
    }

    public TimeBlock(DayOfWeek day, LocalTime startTime, LocalTime endTime) {
        this(day, startTime, Duration.between(startTime, endTime));
    }

    public TimeBlock(DayOfWeek day, LocalTime startTime, Duration duration) {
        this.day = day;
        this.startTime = startTime;
        this.duration = duration;
    }

    public DayOfWeek getDay() {
        return day;
    }

    public void setDay(DayOfWeek day) {
        this.day = day;
    }

    public LocalTime getStartTime() {
        return startTime;
    }
    
    public String startTimeTwelveHour(){
        return startTime.format(DateTimeFormatter.ofLocalizedTime(FormatStyle.SHORT));
    }

    public void setStartTime(LocalTime startTime) {
        this.startTime = startTime;
    }

    public Duration getDuration() {
        return this.duration;
    }

    public void setDuration(Duration duration) {
        this.duration = duration;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public String toString() {
        return day.getDisplayName(TextStyle.SHORT, Locale.US) + " " +
                getStartTime() + "-" +
                getEndTime();
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;

        if (o == null || getClass() != o.getClass()) return false;

        TimeBlock timeBlock = (TimeBlock) o;

        return new EqualsBuilder()
                .append(getDay(), timeBlock.getDay())
                .append(getStartTime(), timeBlock.getStartTime())
                .append(getEndTime(), timeBlock.getEndTime())
                .isEquals();
    }

    @Override
    public int hashCode() {
        return new HashCodeBuilder(17, 37)
                .append(getDay())
                .append(getStartTime())
                .append(getEndTime())
                .toHashCode();
    }


    // Midnight rollover should never happen in this system
    List<TimeBlock> partition(Duration duration) {
        List<TimeBlock> timeBlockList = new ArrayList<>();

        for (LocalTime partitionStartTime = startTime;
             partitionStartTime.isBefore(getEndTime());
             partitionStartTime = partitionStartTime.plus(duration)) {

            LocalTime partitionEndTime = partitionStartTime.plus(duration);

            if (partitionEndTime.isAfter(getEndTime())) {
                partitionEndTime = getEndTime();
            }

            timeBlockList.add(new TimeBlock(day, partitionStartTime, Duration.between(partitionStartTime, partitionEndTime)));
        }

        return timeBlockList;
    }

    public LocalTime getEndTime() {
        return getStartTime().plus(getDuration());
    }

    public void setEndTime(LocalTime endTime) {
        this.setDuration(Duration.between(getStartTime(), endTime));
    }

    public double getHours() {
        return getDuration().getSeconds() / 3600.0;
    }

    public double getMinutes() {
        return getDuration().getSeconds() / 60.0;
    }

    public Duration calculateOverlap(TimeBlock other) {
        if (!getDay().equals(other.getDay()))
            return Duration.ofMinutes(0);

        LocalTime endTime = this.getEndTime().plus(Duration.ofMinutes(1)); // Still count a empty time block as overlapping

        Duration s1e2 = Duration.between(this.getStartTime(), other.getEndTime());
        Duration s2e1 = Duration.between(other.getStartTime(), endTime);

        Duration smallest1 = (s1e2.compareTo(s2e1) < 0)? s1e2 : s2e1;
        Duration smallest2 = (this.getDuration().compareTo(other.getDuration()) < 0)? this.getDuration() : other.getDuration();

        Duration smallest  = (smallest1.compareTo(smallest2) < 0)? smallest1 : smallest2;

        if (smallest.isNegative()) {
            return Duration.ofMinutes(0);
        } else {
            return smallest;
        }
    }
    
    //Just like the calculateOverlap, but ignores day overlap.
    public Duration calculateTimeOverlap(TimeBlock other){
        LocalTime endTime = this.getEndTime().plus(Duration.ofMinutes(1)); // Still count a empty time block as overlapping

        Duration s1e2 = Duration.between(this.getStartTime(), other.getEndTime());
        Duration s2e1 = Duration.between(other.getStartTime(), endTime);

        Duration smallest1 = (s1e2.compareTo(s2e1) < 0)? s1e2 : s2e1;
        Duration smallest2 = (this.getDuration().compareTo(other.getDuration()) < 0)? this.getDuration() : other.getDuration();

        Duration smallest  = (smallest1.compareTo(smallest2) < 0)? smallest1 : smallest2;

        if (smallest.isNegative()) {
            return Duration.ofMinutes(0);
        } else {
            return smallest;
        }
    }

    public Duration calculateOverlap(Collection<TimeBlock> timeBlocks) {
        return timeBlocks.stream()
                .map(this::calculateOverlap)
                .map(Duration::abs)
                .reduce(Duration.ofMinutes(0), Duration::plus);
    }
    
    
}
