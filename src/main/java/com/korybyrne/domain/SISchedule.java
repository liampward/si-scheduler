package com.korybyrne.domain;

import com.korybyrne.domain.course.ClassMeeting;
import com.korybyrne.domain.course.Course;
import com.korybyrne.domain.course.CourseSchedule;
import com.korybyrne.domain.si.SupplementalInstructor;
import java.io.IOException;
import java.math.BigDecimal;
import java.time.DayOfWeek;
import org.optaplanner.core.api.domain.solution.PlanningEntityCollectionProperty;
import org.optaplanner.core.api.domain.solution.PlanningScore;
import org.optaplanner.core.api.domain.solution.PlanningSolution;
import org.optaplanner.core.api.domain.solution.drools.ProblemFactCollectionProperty;
import org.optaplanner.core.api.score.buildin.hardmediumsoft.HardMediumSoftScore;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.stream.Collectors;
import javax.json.Json;
import javax.json.JsonArrayBuilder;
import javax.json.JsonBuilderFactory;
import javax.json.JsonObject;
import javax.json.JsonObjectBuilder;
import javax.json.JsonStructure;

@PlanningSolution
public class SISchedule {
    private Collection<SupplementalInstructor> supplementalInstructors;
    private CourseSchedule courseSchedule;
    private Collection<Session> sessions;

    private HardMediumSoftScore hardMediumSoftScore;

    public SISchedule() {
        supplementalInstructors = new ArrayList<>();
        courseSchedule = new CourseSchedule();
        sessions = new ArrayList<>();
    }

    public void setSupplementalInstructors(List<SupplementalInstructor> supplementalInstructors) {
        this.supplementalInstructors = supplementalInstructors;
    }

    public void setCourseSchedule(CourseSchedule courseSchedule) {
        this.courseSchedule = courseSchedule;
    }

    public void removeEmptySessions() {
        sessions = sessions.stream()
                .filter(session -> session.getDuration().getSeconds() != 0)
                .collect(Collectors.toList());
    }

    public double getSIHoursForCourse(Course course) {
        return getSessions().stream()
                .filter(session -> session.getCourses().contains(course))
                .mapToDouble(Session::getHours)
                .sum();
    }

    public Collection<Session> getSessionsForSI(SupplementalInstructor si) {
        return getSessions().stream()
                .filter(session -> session.getSupplementalInstructor() == si)
                .collect(Collectors.toList());
    }
    
    public CourseSchedule getCourseSchedule(){
        return this.courseSchedule;
    }


    ////////////////////////
    // Optaplanner interop.
    ////////////////////////

    @ProblemFactCollectionProperty
    public Collection<SupplementalInstructor> getSupplementalInstructors() {
        return supplementalInstructors;
    }

    @PlanningEntityCollectionProperty
    public Collection<Session> getSessions() {
        return sessions;
    }

    public void setSessions(List<Session> sessions) {
        this.sessions = sessions;
    }

    @PlanningScore
    public HardMediumSoftScore getHardMediumSoftScore() {
        return hardMediumSoftScore;
    }

    public void setHardMediumSoftScore(HardMediumSoftScore hardMediumSoftScore) {
        this.hardMediumSoftScore = hardMediumSoftScore;
    }

    @ProblemFactCollectionProperty
    public Collection<Course> getCourseList() {
        return courseSchedule.getCourses();
    }

    @ProblemFactCollectionProperty
    public Collection<ClassMeeting> getClassMeetingList() {
        return courseSchedule.getClassMeetingList();
    }
    
    public void addSIs(Collection<SupplementalInstructor> nSupplementalInstructors){
        this.supplementalInstructors.addAll(nSupplementalInstructors);
    }
    
    public void addSessions(Collection<Session> nSessions){
        this.sessions.addAll(nSessions);
    }
    
    public void updateSessionsForSI(SupplementalInstructor si){
        List<Session> oldSessions = new ArrayList<>();
        System.out.println(sessions.toString());
        for(Session session : getSessionsForSI(si)){
            for(Course course : session.getCourses()){
                if(!si.getCourseSchedule().getCourses().contains(course)){
                    oldSessions.add(session);
                }
            }
        }
        System.out.println(oldSessions.toString());
        //Grab the id before sessions gets resized, so no IDs are reused
        int id = sessions.size();
        sessions.removeAll(oldSessions);
        System.out.println(sessions.toString());
        
        for (DayOfWeek day : DayOfWeek.values()) {
                if (day == DayOfWeek.SUNDAY || day == DayOfWeek.SATURDAY) { continue; }
            for (Course course : si.getCourseSchedule().getCourses()) {
                for (int ii = 0; ii < 3; ++ii) {
                    Session session = new Session(day, si, course);
                    session.setId(id++);

                    sessions.add(session);
                }
            }
        }
    }
    
    //Complex Methods
    
    
    public JsonStructure toJson() throws Exception{
        JsonBuilderFactory jsonBuilderFactory = Json.createBuilderFactory(null);
        JsonObjectBuilder jsonBuilder = jsonBuilderFactory.createObjectBuilder();
        JsonArrayBuilder jsonArrayBuilder = jsonBuilderFactory.createArrayBuilder();
        
        for(SupplementalInstructor si : this.supplementalInstructors){
            JsonObjectBuilder siJsonBuilder = jsonBuilderFactory.createObjectBuilder();
            JsonObject jsonSI = null;
            
            siJsonBuilder.add("Name", si.getName());
            siJsonBuilder.add("ID", si.getSsuId());
            
            JsonArrayBuilder requestedCoursesJson = Json.createArrayBuilder();
            for(String course : si.getRequestedClasses()){
                requestedCoursesJson.add(course);
            }
            siJsonBuilder.add("RequestedCourses", requestedCoursesJson.build());
            
            siJsonBuilder.add("MaxRequestedHours", si.getMaximumRequestedHours());
            siJsonBuilder.add("MinRequestedHours", si.getMinimumRequestedHours());
            
            JsonArrayBuilder busyTimesJson = Json.createArrayBuilder();
            for(TimeBlock busyTime : si.getBusyTimes()){
                busyTimesJson.add(
                        Json.createObjectBuilder()
                            .add("Day", busyTime.getDay().toString())
                            .add("StartTime", busyTime.getStartTime().toString())
                            .add("Duration", busyTime.getDuration().toHours())
                            .build());
            }
            siJsonBuilder.add("BusyTimes", busyTimesJson.build());

            JsonArrayBuilder sessionsJson = Json.createArrayBuilder();
            for(Session session : getSessionsForSI(si)){
                if(session.getDuration().isZero())
                    continue;
                JsonArrayBuilder courses = Json.createArrayBuilder();
                
                for(int i = 0; i < session.getCourseCount(); ++i){
                    courses.add(session.getCoursesAsList().get(i).getCourseCode());
                }
                
                sessionsJson.add(
                        Json.createObjectBuilder()
                            .add("Day", session.getDay().toString())
                            .add("StartTime", session.getStartTime().toString())
                            .add("Duration", session.getDuration().toHours())
                            .add("Courses", courses.build())
                            .build());
            }
            siJsonBuilder.add("Sessions", sessionsJson.build());
            
            jsonSI = siJsonBuilder.build();
            jsonArrayBuilder.add(jsonSI);
        }
        
        jsonBuilder.add("SIArray", jsonArrayBuilder.build());
        
        return jsonBuilder.build();
    }
    
}
