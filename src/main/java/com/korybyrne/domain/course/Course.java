package com.korybyrne.domain.course;

import org.jetbrains.annotations.NotNull;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.Objects;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * Class representing a school course, which consists of multiple {@link Section}s, which themselves consist of multiple {@link ClassMeeting}s.
 */
public class Course {
    private String department;
    private String codeNumber;
    private String name;

    private Collection<Section> sections;

    /**
     * Constructs an empty {@link Course} object, with no sections and an empty string for the department, code number, and name.
     */
    public Course() {
        this.department = "";
        this.codeNumber = "";
        this.name = "";

        this.sections = new ArrayList<>();
    }

    /**
     * Gets the department code of the course.
     * @return the department code of the course.
     */
    public String getDepartment() {
        return department;
    }

    /**
     * Gets the code number of the course.
     * @return the code number of the course.
     */
    public String getCodeNumber() {
        return codeNumber;
    }

    /**
     * Gets the name of the course.
     * @return the name of the course.
     */
    public String getName() {
        return name;
    }

    /**
     * Sets the name of the course.
     * @param name the name of the course.
     */
    public void setName(String name) {
        this.name = name;
    }

    /**
     * Gets an unmodifiable reference to the sections {@link Collection}
     * @return an unmodifiable reference to the sections {@link Collection}
     */
    public Collection<Section> getSections() {
        return Collections.unmodifiableCollection(this.sections);
    }

    /**
     * Gets the course code for this course.
     * Equivalent to the concatenation of {@link #getDepartment()} and {@link #getCodeNumber()}
     * @return the course code for this course.
     */
    public String getCourseCode() {
        return department + codeNumber;
    }

    /**
     * Sets the course code for this course.
     * @param courseCode the course code for this course.
     * @throws IllegalArgumentException when the course code does not have a department code portion and a code number portion.
     */
    public void setCourseCode(@NotNull String courseCode) {
        Pattern codePattern = Pattern.compile("\\d+");
        Matcher matcher = codePattern.matcher(courseCode);

        if (matcher.find() && matcher.start() != 0) {
            int codeIndex = matcher.start();

            this.department = courseCode.substring(0, codeIndex);
            this.codeNumber = courseCode.substring(codeIndex);
        } else {
            throw new IllegalArgumentException("Parameter <courseCode> must have a department code portion AND a code number portion: " + courseCode);
        }
    }

    /**
     * Gets the number of sections this course has.
     * Equivalent to {@link #getSections()}.size().
     * @return the number of sections this course has.
     */
    public int getSectionCount() {
        return getSections().size();
    }


    // complex methods

    /**
     * Adds a section to this course, setting the section's parent.
     * @param section the section to add.
     */
    public void addSection(Section section) {
        section.setParent(this);
        sections.add(section);
    }


    @Override
    public String toString() {
        return getCourseCode();
    }
    
    @Override
    public boolean equals(Object o){
        if(o == null)
            return false;
        if(o == this)
            return true;
        if(!(o instanceof Course))
            return false;
        
        
        Course c = (Course) o;
        return this.getCourseCode().equals(c.getCourseCode());
    }

    @Override
    public int hashCode() {
        int hash = 7;
        hash = 11 * hash + Objects.hashCode(this.department);
        hash = 11 * hash + Objects.hashCode(this.codeNumber);
        return hash;
    }

    /**
     * Helper method for returning a detailed string representation of the course, including a representation of each {@link Section} and {@link ClassMeeting} therein.
     * @return a detailed representation of this course.
     */
    public String toStringDetailed() {
        StringBuilder sb = new StringBuilder().append(getCourseCode()).append(":").append('\n');

        for (Section section : getSections()) {
            sb.append('\t').append(section.toStringDetailed()).append('\n');
        }

        return sb.toString();
    }
}
