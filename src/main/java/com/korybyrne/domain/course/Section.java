package com.korybyrne.domain.course;

import java.util.ArrayList;
import java.util.Collection;

/**
 * Class representing a specific section of a school course, including all class meetings related to that section.
 */
public class Section {
    private Course parent;

    private String number;
    private String status;

    private Collection<ClassMeeting> classMeetings;

    /**
     * Constructs an empty {@link Section} object.
     */
    public Section() {
        this.parent = null;
        this.number = "";
        this.status = "";
        this.classMeetings = new ArrayList<>();
    }


    /**
     * Gets the parent of this section
     * @return
     */
    Course getParent() {
        return parent;
    }

    void setParent(Course parent) {
        this.parent = parent;
    }

    public String getNumber() {
        return number;
    }

    public void setNumber(String number) {
        this.number = number;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public Collection<ClassMeeting> getClassMeetings() {
        return classMeetings;
    }

    // complex methods

    public void addClassMeeting(ClassMeeting meeting) {
        meeting.setParent(this);
        getClassMeetings().add(meeting);
    }

    public void addClassMeetings(Collection<ClassMeeting> meetings) {
        meetings.forEach(this::addClassMeeting);
    }

    public boolean isCancelled() {
        return status.equals("canceled");
    }

    // overrides

    @Override
    public String toString() {
        return parent + "-" + number;
    }

    public String toStringDetailed() {
        StringBuilder sb = new StringBuilder().append(getNumber()).append(":").append('\n');

        for (ClassMeeting meeting : getClassMeetings()) {
            sb.append("\t\t").append(meeting.toString()).append('\n');
        }

        return sb.toString();
    }
}
