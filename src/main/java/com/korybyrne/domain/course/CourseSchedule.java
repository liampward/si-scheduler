package com.korybyrne.domain.course;

import java.util.*;
import java.util.stream.Collectors;

public class CourseSchedule {
    private Map<String, Course> courseMap;

    public CourseSchedule() {
        this(new ArrayList<>());
    }

    public CourseSchedule(List<Course> courseList) {
        this.courseMap = new TreeMap<>(); // TreeMap for easier print debugging
        this.addCourses(courseList);
    }

    // complex methods

    public Optional<Course> getCourse(String departmentCode) {
        return Optional.ofNullable(courseMap.get(departmentCode));
    }
    
    public List<Course> getCourseSubset(List<String> courseCodes){
        return courseMap.values().stream()
                .filter(c -> courseCodes.contains(c.getCourseCode()))
                .collect(Collectors.toList());
    }

    public void addCourse(Course course) {
        courseMap.put(course.getCourseCode(), course);
    }

    public void addCourses(Collection<Course> courses) {
        courses.forEach(this::addCourse);
    }

    public Collection<Course> getCourses() {
        return courseMap.values();
    }

    public List<ClassMeeting> getClassMeetingList() {
        return courseMap.values().stream()
                .map(Course::getSections).flatMap(Collection::stream)
                .map(Section::getClassMeetings).flatMap(Collection::stream)
                .collect(Collectors.toList());
    }

    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();

        for (Course course : courseMap.values()) {
            sb.append(course.toString()).append('\n');
        }

        return sb.toString();
    }
}
