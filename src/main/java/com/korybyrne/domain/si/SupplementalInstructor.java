package com.korybyrne.domain.si;

import com.korybyrne.domain.TimeBlock;
import com.korybyrne.domain.course.Course;
import com.korybyrne.domain.course.CourseSchedule;

import java.util.*;

public class SupplementalInstructor {
    private String name;
    private int ssuId;

    private RequestedHours requestedHours;

    private Collection<TimeBlock> busyTimes;
    private CourseSchedule courseSchedule;
    
    private TreeSet<String> requestedClasses;
    private boolean locked;

    public SupplementalInstructor(String name, int ssuId,
                                  RequestedHours requestedHours,
                                  List<TimeBlock> busyTimeList, List<Course> courseList,
                                  TreeSet<String> requestedClasses) {
        this.name = name;
        this.ssuId = ssuId;
        this.requestedHours = requestedHours;
        this.requestedClasses = requestedClasses;

        this.busyTimes = new ArrayList<>();
        this.courseSchedule = new CourseSchedule();

        this.busyTimes.addAll(busyTimeList);
        this.courseSchedule.addCourses(courseList);
        
        locked = false;
    }

    public int getSsuId() {
        return ssuId;
    }
    
    public void setSsuId(int nID){
        this.ssuId = nID;
    }
    
    public String getName() {
        return name;
    }
    
    public void setName(String nName){
        this.name = nName;
    }

    public Collection<TimeBlock> getBusyTimes() {
        return busyTimes;
    }

    public CourseSchedule getCourseSchedule() {
        return courseSchedule;
    }
    
    public boolean getLocked(){
        return this.locked;
    }
    
    public void setLocked(boolean nLocked){
        this.locked = nLocked;
    }
    
    public void setRequestedHours(RequestedHours nRequestedHours){
        this.requestedHours = nRequestedHours;
    }
    
    @Override
    public String toString() {
        return name;
    }

    //////////////////
    // complex methods
    //////////////////

    //TODO:kb: generalize, load in from spreadsheet
    public Set<String> getSessionTypes() {
        return new TreeSet<>(Arrays.asList("MATH2110", "MATH2130", "ENGL1101"));
    }

    public void addBusyTime(TimeBlock busyTime) {
        busyTimes.add(busyTime);
    }

    public double getMaximumRequestedHours() {
        return requestedHours.getMaximum().getSeconds() / 3600.0;
    }

    public double getMinimumRequestedHours() {
        return requestedHours.getMinimum().getSeconds() / 3600.0;
    }
    
    public Collection<String> getRequestedClasses(){
        return this.requestedClasses;
    }
    
    public void setRequestedClasses(List<Course> nCourses){
        TreeSet<String> nCoursesString = new TreeSet<>();
        nCourses.forEach(course -> nCoursesString.add(course.getCourseCode()));
        
        this.requestedClasses = nCoursesString;
        this.courseSchedule = new CourseSchedule(nCourses);
        
    }
    
    @Override
    public boolean equals(Object other){
        
        if(other == this)
            return true;
        
        if(!(other instanceof SupplementalInstructor))
            return false;
        
        SupplementalInstructor otherSI = (SupplementalInstructor)other;
        if(otherSI.getSsuId() == this.getSsuId())
            return true;
        
        return false;
    }
}
