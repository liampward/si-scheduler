package com.korybyrne.domain.si;

import java.time.Duration;

public class RequestedHours {
    private Duration minimum;
    private Duration maximum;

    public RequestedHours(Duration minimum, Duration maximum) {
        this.minimum = minimum;
        this.maximum = maximum;
    }

    public Duration getMinimum() {
        return minimum;
    }

    public Duration getMaximum() {
        return maximum;
    }
}
